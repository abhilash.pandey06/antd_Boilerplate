const dbc =require('../dbc');

 function success(msg, obj = {}) {
	obj.success = true
	obj.msg = msg
	return obj
}

 function fail(msg, obj = {}) {
	obj.success = false
	obj.msg = msg
	return obj
}

async function apikeyValidation (req, obj= {}) {
	let { apikey } = req.headers;
	if (apikey){
		let conn = await dbc.myDB();
		let user = await conn.collection('users').find({'apikey': apikey}).toArray();
		return user.length > 0 ? success('apikey validation success'): fail('user not found in database');
	}
	else {
		return fail('apikey not found in request headers')
	}
}
module.exports={apikeyValidation,fail,success}