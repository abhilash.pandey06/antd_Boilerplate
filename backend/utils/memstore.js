// memstore.js
import config from '../config'
import redis from 'redis'
import bluebird from 'bluebird'

// promisify redis client
bluebird.promisifyAll(redis.RedisClient.prototype)
bluebird.promisifyAll(redis.Multi.prototype)


export default class MemStore {

    constructor(namespace) {
        
        // create a new client for the plugin
        this.client = redis.createClient({
            prefix: `${namespace}:`,
            ...config.redis
        })
        
        // on connect
        this.client.on('connect', () => {
            console.log(`Created redis client for scope '${ namespace }'`)
        })
        
        // proxify this class to be implemented later
        return new Proxy(this, {
            async get(target, name) {
                // if name is not a property of target
                if(!target[ name ]) {

                    let redis_key = name
                    let type = await target.client.typeAsync(redis_key) // detect type

                    if( type === 'none' ) return null
                    
                    if(type === 'string' || type === 'number') // return simple type 
                        return await target.client.getAsync(redis_key)

                    if(type === 'hash') {
                        let val = await target.client.hgetallAsync(redis_key)
                        
                        // return proxied result so that we can capture adding values 
                        return new Proxy(val, {
                            set(_target, hkey, value) {
                                return target.client.hsetAsync(redis_key, hkey, value)
                            }
                        })
                    }

                    if(type === 'list') {
                        let val = await target.client.lrangeAsync(redis_key, 0, 50) // retrieve the entire list

                        return new Proxy(val, {
                            get(array, property, receiver) {
                                // custom methods
                                if(property === 'fetch') {
                                    return async (offset, limit) => {
                                        let items = await target.client.lrangeAsync(redis_key, offset, limit)
                                        array.splice(0)
                                        items.forEach(i => array.push(i))
                                    }
                                }
                                
                                // returns actual length of the redis list
                                if(property === 'lengthAsync')
                                    return async () => await target.client.llenAsync(redis_key)


                                // array methods override
                                if(typeof array[property] === 'function') {
                                    if(property === 'push') {
                                        return (item) => {
                                            target.client.rpushAsync(redis_key, item) // push to redis
                                            array[property].call(receiver, item) // push to array
                                        }
                                    }
                                    if(property === 'pop') {
                                        return () => {
                                            target.client.rpopAsync(redis_key) // pop from redis
                                            array[property].call(receiver) // pop from array
                                        }
                                    }
                                    if(property === 'unshift') {
                                        return (item) => {
                                            target.client.lpushAsync(redis_key, value) //push to redis
                                            array[property].call(receiver, item)
                                        }
                                    }
                                    if(property === 'shift') {
                                        return () => {
                                            target.client.lpopAsync(redis_key) // pop from redis
                                            array[property].call(receiver)
                                        }
                                    }
                                    
                                    // native array methods
                                    return array[property].bind(array)
                                }

                                return array[property]
                            },
                            
                            // setting array items will set corresponding redis list item
                            set(target, property, value) {
                                // if index
                                if(/^\d$/.test(property)) {
                                    let index = Number.parseInt(property)

                                    if(index < target.length) target[index] = value

                                    return target.client.llenAsync(redis_key).then(length => {
                                        if(index < length)
                                            return target.client.lsetAsync(redis_key, index, value)
                                    })
                                }

                                target[property] = value
                                return true 
                            }
                        })
                    }

                    if(type === 'set') {
                        let val = await target.client.smembersAsync(redis_key)
                        return new Proxy(val, {
                            get(set, property, receiver) {
                                if(typeof set[property] === 'function') {
                                    if(property === 'add') {
                                        return (item) => {
                                            target.client.sadd(redis_key, item)
                                            set[property].call(set)
                                        }
                                    }
                                    if(property === 'delete') {
                                        return (item) => {
                                            target.client.srem(redis_key, item)
                                            set[property].call(set, item)
                                        }
                                    }
                                }
                                return set[property].bind(receiver)
                            }
                        })
                    }
                }
                return target[name]
            },
            
            /* replace redis key value */
            async set(target, property, value) {
                let { client } = target
                
                // STRING / NUMBER
                if(typeof value === 'string' || typeof value === 'number') {
                    return await client.setAsync(property, value)
                }
                
                // ARRAY
                if(typeof value === 'object' && value.constructor.name === 'Array') {
                    await client.delAsync(property)
                    return await client.rpushAsync(property, value)
                }

                // SET 
                if(typeof value === 'object' && value.constructor.name === 'Set') {
                    await client.delAsync(property)
                    let updates = [] // to store the promises
                    
                    for(let item of value)
                        updates.push(client.saddAsync(property, item)) 

                    return await updates
                }

                // HASH
                if(typeof value === 'object') {
                    await client.delAsync(property)
                    return await client.hmsetAsync(property, value)
                }

                return true
            }
        })
    }
}

