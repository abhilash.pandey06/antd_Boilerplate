//import promise object to create promises
const Promise =require('bluebird');
//used Mongoclient to connect with databases
const MongoClient= require('mongodb'),
     ObjectId=require('mongodb')
//Database connection string to connect Database
const connectionString='mongodb://localhost:27017/mydb';

//databases objects
let database = {
    //databases ananomyous function to connect databases
  myDB: () => {
    return new Promise((resolve, reject) => {
        //Establising the connection with mongodb data
        MongoClient.connect(connectionString).then(conn=> {
          resolve(conn)
        }).catch(err => {
            console.log(err);
          reject(err)
        })
    })
  },
  objectify: (id)=>{
    return ObjectId(id)
  }
}
//export the databses objects
module.exports=  database;



