const adminClient =require('keycloak-admin-client');
const getToken =require('keycloak-request-token');
const request =('request-promise-native');
const config =require('../config');
const requestor =require('request')
const axios =require('axios')
const {sendVerificationEmail,sendNewPassword,mailInviteToUsers,}=require('./emailUtility');
const dbc= require('../dbc')

const moment =require('moment')

class AuthUtils {
    constructor() {
        this.request = new KeyCloakAdminRequest(config.keycloak);
    }

    //#3 Validate the Login form with username or password
    validateCredentials(userinfo,cb){
        let formData = {
            "grant_type":"password",
            "client_id":config.auth_client_id,
            "username":userinfo.username,
            "password":userinfo.password
        }
        var requestor = require("request");

        var options = { method: 'POST',
          url: config.auth_url+'realms/apiman/protocol/openid-connect/token',
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
          form: formData
        };
        requestor(options, function (error, response, body) {
          if (error) throw new Error(error);
          cb( JSON.parse(body) )
        });
    }
    //#4 
    async validateAuthCode(authCode){
        let conn = await dbc.myDB()
        let authObj = await conn.collection("authorization_codes").find({"code":authCode}).toArray()
        if(authObj.length===0){
            throw new Error("Invalid Auth Code")
        }
        let curTime = moment.unix()

        if(curTime>authObj[0].expires){
            throw new Error("link has expired")
        }
        else{
            this.enableUser(authObj[0].username).then(()=>{
                return {status:1}
            })
            .catch(err=>{
                throw new Error(err.message)
            })
        }
    }

    //#1 Login Form forget password auth utility code
    async forgotPassword(username){
        try {
            let user  = await this.findUser(username)
            let client = await adminClient(config.keycloak)
            let password = Math.random().toString(36).slice(-8)
            let reset = await resetUserPassword(client, config.keycloak.realm, user, password)
            let email = await sendNewPassword(username, password)
            return {status:1}
        } catch (error) {
            throw new Error(error.message)    
        }
    }

    // #2 Login Form Reset Password auth utility code
    async resetThePassword(info,cb) {
        this.validateCredentials(info,async(status)=>{
            console.log(status)
            if(status.access_token){
                console.log(status.id)
                try {
                    let user  = await this.findUser(info.username)
                    let client = await adminClient(config.keycloak)
                    let password = info.newPassword
                    let reset = await resetUserPassword(client, config.keycloak.realm, user, password)
                    cb({status:1})
                } catch (error) {
                    throw new Error(error.message)    
                }

            }
            else{
                cb({status:-1})
            }

        })
    }
    realmsList() {
        return adminClient(config.keycloak).then(client => client.realms.find());
    }

    usersList() {
        return adminClient(config.keycloak).then(client => client.users.find(config.keycloak.realm));
    }

    createTestUser(userinfo) {
        return adminClient(config.keycloak)
            .then(
                client => createTestUser(client, config.keycloak.realm, userinfo)
                    .then(
                        newUser => resetUserPassword(client, config.keycloak.realm, newUser, userinfo.password)
                            .then(
                                sendVerificationEmail(userinfo).then(
                                    () => newUser
                                )
                                
                            )
                    )
            );
    }

    createNewUser(userInfo) {
        return adminClient(config.keycloak)
            .then(
                client => createNewUser(client, config.keycloak.realm, userInfo)
                    .then(
                        newUser => resetUserPassword(client, config.keycloak.realm, newUser, "test")
                            .then(
                                () => newUser
                            )
                    )
            )
    }

    enableUser(username) {
        return adminClient(config.keycloak)
            .then(
                client => this.findUser(username)
                    .then(
                        user => {
                            user.emailVerified = true;
                            user.enabled = true;
                            return client.users.update(config.keycloak.realm, user)
                                .then(
                                    () => 'user updated'
                                );
                        }
                    )
            );
    }

    findUser(username) {
        return adminClient(config.keycloak)
            .then(
                client => client.users.find(config.keycloak.realm, {
                    username: username
                })
            )
            .then(
                users => {
                    let user = users && users[0];
                    return user && user.id ? Promise.resolve(user) : Promise.resolve('user not found');
                }
            );
    }

    setTestUserCustomerId() {
        return adminClient(config.keycloak)
            .then(
                client => this.findTestUser()
                    .then(
                        user => {
                            user.attributes = user.attributes || {};
                            user.attributes.customerId = 123;
                            return client.users.update(config.keycloak.realm, user)
                                .then(
                                    () => 'customerId added'
                                );
                        }
                    )
            );
    }

    removeTestUserCustomerId() {
        return adminClient(config.keycloak)
            .then(
                client => this.findTestUser()
                    .then(
                        user => {
                            user.attributes = user.attributes || {};
                            user.attributes.customerId = undefined;
                            return client.users.update(config.keycloak.realm, user)
                                .then(() => 'customerId removed');
                        }
                    )
            );
    }

    // this is an example how to get user by id
    getUserById() {
        return adminClient(config.keycloak)
            .then(
                client => this.findTestUser()
                    .then(
                        user => client.users.find(config.keycloak.realm, {
                            userId: user.id
                        })
                    )
            );
    }

    deleteTestUser() {
        return adminClient(config.keycloak)
            .then(
                client => this.findTestUser()
            )
            .then(
                user => this.deleteUserById(user.id)
            );
    }

    deleteUserById(userId) {
        return adminClient(config.keycloak)
            .then(
                client => client.users.remove(config.keycloak.realm, userId)
            ).then(
                () => 'user deleted'
            );
    }

    // admin client doesn't have these methods

    createRole() {
        return this.authenticate()
            .then(
                token => this.request.createRole('TEST_ROLE', token)
            )
            .then(
                () => 'role created'
            );
    }

    deleteRole() {
        return this.authenticate()
            .then(
                token => this.request.deleteRole('TEST_ROLE', token)
            )
            .then(
                () => 'TEST_ROLE role is deleted'
            );
    }

    addTestRoleToTestUser() {
        return this.findTestUser()
            .then(
                user => this.authenticate()
                    .then(
                        token => this.getRoleByName('TEST_ROLE')
                            .then(
                                role => this.request.addRole(user.id, role, token)
                            )
                    ).then(
                        () => 'TEST_ROLE role is added to the user login=test_user'
                    )
            );
    }

    removeTestRoleFromTestUser() {
        return this.findTestUser()
            .then(
                user => this.authenticate()
                    .then(
                        token => this.getRoleByName('TEST_ROLE')
                            .then(
                                role => this.request.removeRoleFromUser(user.id, role, token)
                            )
                    )
                    .then(
                        () => 'TEST_ROLE role is removed from user'
                    )
            );
    }

    getRoleByName(roleName) {
        return this.authenticate()
            .then(
                token => this.request.getRole(roleName, token)
            )
            .then(
                role => role ? Promise.resolve(role) : Promise.reject('role not found')
            );
    }

    authenticate() {
        return getToken(config.keycloak.baseUrl, config.keycloak);
    }

}
//***************************************** *****************************************/
function createNewUser(client, realm, userInfo) {
    return client.users.create(realm, {
        username: userInfo.username,
        email: userInfo.username,
        enabled: true,
        emailVerified: true
    })
}

 function createTestUser(client, realm, userinfo) {
    return client.users.create(realm, {
        username: userinfo.username,
        email:userinfo.username,
        enabled: false
    });
}

 function resetUserPassword(client, realm, user, password) {
    // set password 'test_user' for a user
    return client.users.resetPassword(realm, user.id, {
        type: 'password',
        value: password
    });
}
//***************************************** *****************************************/
class KeyCloakAdminRequest {

    constructor(config) {
        config.keycloak = config;
    }

    addRole(userId, role, token) {
        return this.doRequest('POST',
            `/admin/realms/${config.keycloak.realm}/users/${userId}/role-mappings/realm`, token, [role]);
    }

    createRole(roleName, token) {
        return this.doRequest('POST',
            `/admin/realms/${config.keycloak.realm}/roles`, token, {
                name: roleName
            });
    }

    deleteRole(roleName, token) {
        return this.doRequest('DELETE',
            `/admin/realms/${config.keycloak.realm}/roles/${roleName}`, token);
    }

    getRole(roleName, token) {
        return this.doRequest('GET',
            `/admin/realms/${config.keycloak.realm}/roles/${roleName}`, token, null);
    }

    removeRoleFromUser(userId, role, token) {
        return this.doRequest('DELETE',
            `/admin/realms/${config.keycloak.realm}/users/${userId}/role-mappings/realm`, token, [role]);
    }

    doRequest(method, url, accessToken, jsonBody) {
        let options = {
            url: config.keycloak.baseUrl + url,
            auth: {
                bearer: accessToken
            },
            method: method,
            json: true
        };

        if (jsonBody !== null) {
            options.body = jsonBody;
        }

        return request(options).catch(error => Promise.reject(error.message ? error.message : error));
    }
}

module.exports = AuthUtils;