// activityUtility.js

const dbc = require('../dbc')
const config = require('../config')
const { ObjectId } = require('mongodb')
const moment = require('moment')
const nodemail = require('nodemailer')
const {makeTemplate,invite} = require('../emailInvite');
const uuidv1 = require('uuid/v1')

let daystamp = moment().format('DD-MM-YYYY');


//nodemailer to send verification email
async function sendVerificationEmail(userinfo) {
	// let htmlBody = invite.blankTemplate
	let conn = await dbc.myDB()
	let transporter = nodemail.createTransport({
		host: 'smtp.gmail.com',
		port: 587,
		secure: false, // secure:true for port 465, secure:false for port 587
		auth: {
			user: config.smtpAuth.user,
			pass: config.smtpAuth.pass
		}
	})
	let uniquecode = uuidv1()
	let authUrl = config.apiUrl + "?auth=" + uniquecode + "/#" + userinfo.hashHistory
	let expires = moment().add(1, 'hours').unix()

	let authBody = { username: userinfo.username, authUrl: authUrl, code: uniquecode, expires: expires }
	let authUpdate = await conn.collection("authorization_codes").update({ "username": userinfo.username }, { "$set": authBody }, { upsert: true })
	let messageBody = `Thank you for registering with us. Please verify the email by clicking on the link below.<br/><a href='${authUrl}'>${authUrl}</a> `
	let htmlBody = makeTemplate(messageBody)
	let mailOptions = {
		from: "Team Prabandhak",
		to: [userinfo.username],
		subject: "Verify your email",
		html: htmlBody
	}
	transporter.sendMail(mailOptions, (error, info) => {
		if (error) {
			console.log("Not Mailed", error)
			return { status: 0 }
		}
		else {
			console.log("Mailed")
			return { status: 1 }
		}

	});
}

//Send New password Notification link
async function sendNewPassword(username, password) {
	let transporter = nodemail.createTransport({
		host: 'smtp.gmail.com',
		port: 587,
		secure: false, // secure:true for port 465, secure:false for port 587
		auth: {
			user: config.smtpAuth.user,
			pass: config.smtpAuth.pass
		}
	})

	let messageBody = `We recieved a request to reset your password. Here are your new credentials, 
					   <br/><strong>username : ${username}</strong><br/>
					   <strong>password : ${password}</strong><br/>
					   `
	let htmlBody = makeTemplate(messageBody)
	let mailOptions = {
		from: "Team Prabandhak",
		to: [username],
		subject: "Password Reset Request",
		html: htmlBody

	}
	return transporter.sendMail(mailOptions, (error, info) => {
		if (error) {
			console.log("Not Mailed", error)
			return { status: 0 }
		}
		else {
			console.log("Mailed")
			return { status: 1 }
		}
	});
}

module.exports = {sendVerificationEmail,sendNewPassword,}

