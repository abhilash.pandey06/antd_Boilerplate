// Handling the keycloak Auth

const express =require('express');
// const success =require('../utils/apiHelpers'),
//       fail=require('../utils/apiHelpers')
const {success,fail} = require('../utils/apiHelpers');
let AuthUtil =require('../models/authUtility')
let auth = new AuthUtil({})

let router = express.Router()

router.get("/*", (req,res)=>{

    res.send({})
})
//Route to Resiter user
router.post("/register/user", (req,res)=>{
    auth.createTestUser(req.body)
    .then(s=>{
        res.send(s)
    })
    .catch(err=>{
        res.send(err)
    })
})
//Route to user Login Enable to login 
router.post("/enable/user", (req,res)=>{
    auth.enableUser(req.body.username)
    .then(s=>{
        res.send(s)
    })
    .catch(err=>{
        res.send(err)
    })
})
//Route to user Login
router.post("/login/user", (req,res)=>{
    auth.validateCredentials(req.body, (data)=>{
        res.send(data)
    })
    
})

router.post("/validate/auth-code", (req,res)=>{
    auth.validateAuthCode(req.body.authCode)
    .then(s=>{
        res.send(success(s))
    })
    .catch(err=>{
        res.send(fail(err.message))
    })
})
//Route user 
router.post("/resend/verification", (req,res)=>{
    auth.validateAuthCode(req.body.username)
    .then(s=>{
        res.send(success(s))
    })
    .catch(err=>{
        res.send(fail(err.message))
    })
})
//Route to User forget password 
router.post("/forgot/password", (req,res)=>{
    auth.forgotPassword(req.body.username)
    .then(s=>{
        res.send(success(s))
    })
    .catch(err=>{
        res.send(fail(err.message))
    })
})
//Route User Reset password 
router.post("/reset/password", (req,res)=>{
    auth.resetThePassword(req.body, (s)=>{
        if(s.status===1)
        res.send(success(s))
        else
        res.send(fail(s))
    })
})
/********************************** FACE BOOK LOGIN API ***********************************/
//Route User isfb password 
router.post("/isfb/login", (req,res)=>{
    auth.resetThePassword(req.body, (s)=>{
        if(s.status===1)
        res.send(success(s))
        else
        res.send(fail(s))
    })
})

router.post("/isfb/register", (req,res)=>{
    auth.validateAuthCode(req.body.authCode)
    .then(s=>{
        res.send(success(s))
    })
    .catch(err=>{
        res.send(fail(err.message))
    })
})
/********************************GOOGLE LOGIN API*********************************** */

//Route User isfb password 
router.post("/isgoogle/login", (req,res)=>{
    auth.resetThePassword(req.body, (s)=>{
        if(s.status===1)
        res.send(success(s))
        else
        res.send(fail(s))
    })
});



module.exports =router