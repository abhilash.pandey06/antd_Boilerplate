const backend_url = 'http://localhost:3001'
const config = {
	baseUrl:backend_url,
	port : "3001",
  	prabandhakDb : {
		db_host :'localhost',
		db_port : '27017',
		db_name : 'mydb',
		db_username : '',
		db_password : ''
	},
	keycloak:{
		realm: "apiman",
		baseUrl: "http://beta.auth.revup.reverieinc.com/auth",
		resource: "rev_data_manager",
		username: "keycloakadmin",
		password: "keycloak@reverie123!",
		grant_type: 'password',
		client_id: 'admin-cli'
	},
	nlpy:"http://localhost:5001/",
	xliffUtils:"http://localhost:8732/",
	socketUrl:'ws://localhost:4000',
    defaultLangMap : ["hindi", "gujarati", "marathi","odia", "assamese", "bengali", "kannada", "telugu", "tamil", "malayalam", "punjabi"],
	smtpAuth:{"user":"prabandhak@reverieinc.com", "pass":"pRaBaNdHaK123"},
	evaluationTaskName:"prabandhak_evaluation_task",
	superadminkey:"prabandhak_super_admin_key",
	evaluationTaskNameDecorated:"Evaluation Test",
	evaluationTaskDescription:"This test consists of english sentences from different categories like News, Entertainment, E-Commerce, Education etc. Please translate the sentences carefully. Scoring 90% and above accuracy in this test will allow you access to all paid jobs. All the best..!!",
	aws_access_key_id:"AKIAIL75SIZRDVRJPYLA",
	aws_secret_access_key:"pbmEQ3Vs42lZX8NDwoJdSzn/39cau8Jt/EViNxxd",
	aws_region : "ap-southeast-1",
	aws_bucket:"rev-prab-data",
	sampleTermbaseFile:backend_url+"/core/termbase/sample",
	mailInviteUrl: backend_url+"/#/team/register?apikey=",
	taskCreationMailUrl: backend_url+"/#/projects",
	apiUrl:backend_url+"/",
	auth_client_id:"rev_data_manager",
	auth_url:"http://beta.auth.revup.reverieinc.com/auth/",
	recompilable_file_types:["docx","srt","html","htm","txt"],
	imageEndpoint: "http://rev-prab-data.s3-ap-southeast-1.amazonaws.com/Profile-Images/",
	stringObject : { 
					"confirmed" : false, 
					"assigned_to" : "", 
					"candidates" : [],
					"saved_for":[],
					"skipped_by":[],
					"proof_readable":false,
					"current_level":0, 
					"maxProofreads":0 
				},
	termbaseKeys : {'instruction': '', 'term':'','hindi':'', 'gujarati':'', 'marathi':'','odia':'', 'assamese':'', 'bengali':'', 'kannada':'', 'telugu':'', 'tamil':'', 'malayalam':'', 'punjabi':'', 'tb_name': '', 'createdBy': ''},
	supportedFileFormats: ['docx', 'xlsx', 'pptx', 'odt', 'ott', 'ods', 'ots', 'odp', 'otp', 'html', 'htm', 'xhtml', 
	'sdlxliff', 'xliff','po','ttx', 'mif', 'idml', 'icml', 'dita', 'csv', 'tsv', 'xml', 'dtd', 'json', 'yaml', 'txt',
	'properties', 'resx', 'strings', 'srt', 'wix' ],
	availableCredits: 1000000,
	min_mt_volume:25000,
	minimum_evaluation_string_count:100,
	tncUrl:'https://reverieinc.com/privacy-policy.html',
	project_object: {
		'apikey':'',
		'project_name':'',
		'client_project_name':'',
		'task_names':[],
		'languages':[],
		'maxProofreads':0,
		'type':'segment',
		'memory_stats':{},
		'preprocessed_word_count':0,
		'wordCount': 0,
		'created_at':''

	},
	task_object : { 
	    "name" : "", 
	    "total" : 0, 
	    "languages" : [], 
		"stats" : {},
		"memory_stats":{}, 
	    "team" : {},
		"status" : "processing", //[]
		"wordCount":0,
		"points":0,
		"autoAssign":false,
		"context":[],
		"deadline":"",
		"type": "segment",
		"maxProofreads":0,
		"adminDescription":"",
		"clientTaskname":"",
		"clientDescription":"",
		"original_file_download": false,
		"apikey":"",
		"segmentStats": {
			total: 0,
			conflicts: 0,
			noConflicts: 0,
			errors: 0,
			markedWithError: 0
		},
		"invitedUsers":[],
		"applicants":[],
		"preprocess":false,
		"preprocessed_word_count":0,
		"tbList":[], //List of termbases to be applied
		"tmList":[] //List of translation memories to be applied
		}
}

module.exports =config;
