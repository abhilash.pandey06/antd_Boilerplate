//import express modules
const express =require('express');
//initiate express as app
const app =express();
//import all routes 
const routes =require('./routes/allRouters');
const config =require('./config');
//defined port 
const port=config.port;
//import bodyParse modules
const bodyParser=require('body-parser');


//included middleware dependencies.
app.use(bodyParser.json({ limit: '100mb' }));
app.use(bodyParser.urlencoded({ exteneded: false }));
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// All Routes route in url
Object.keys(routes).forEach(key => {
  app.use(key, routes[key])
})

//for listing server on port 8000 
app.listen(port,function(){
    console.log("Backend listning in port ",+port);
})