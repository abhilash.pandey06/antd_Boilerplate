let messageBody =""
 function makeTemplate(msg){
	messageBody = msg
	let updated = invite.blankTemplate.replace("!!!EEETTT!!!", msg)
	return updated
}

const invite = {

	blankTemplate:`<!-- THIS EMAIL WAS BUILT AND TESTED WITH LITMUS http://litmus.com -->
    <!-- IT WAS RELEASED UNDER THE MIT LICENSE https://opensource.org/licenses/MIT -->
    <!-- QUESTIONS? TWEET US @LITMUSAPP -->
    <!DOCTYPE html>
    <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <meta charset="utf-8"> <!-- utf-8 works for most cases -->
        <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
        <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
      <!--   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous"> -->
        <script defer src="https://use.fontawesome.com/releases/v5.0.12/js/all.js" integrity="sha384-Voup2lBiiyZYkRto2XWqbzxHXwzcm4A5RfdfG6466bu5LqjwwrjXCMBQBLMWh7qR" crossorigin="anonymous"></script>
        <title>Event - [Plain HTML]</title> <!-- The title tag shows in email notifications, like Android 4.4. -->
    
        <!-- Web Font / @font-face : BEGIN -->
        <!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->
    
        <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
        <!--[if mso]>
            <style>
                * {
                    font-family: Arial, sans-serif !important;
                }
            </style>
        <![endif]-->
    
        <!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
        <!--[if !mso]><!-->
            <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet"> -->
        <!--<![endif]-->
    
        <!-- Web Font / @font-face : END -->
    
        <!-- CSS Reset -->
        <style>
    
            /* What it does: Remove spaces around the email design added by some email clients. */
            /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
            html,
            body {
                margin: 0 auto !important;
                padding: 0 !important;
                height: 100% !important;
                width: 100% !important;
    
            }
            
            /* What it does: Stops email clients resizing small text. */
            * {
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
                font-family: monospace !important;
            }
    
            /* What it does: Centers email on Android 4.4 */
            div[style*="margin: 16px 0"] {
                margin:0 !important;
            }
    
            /* What it does: Stops Outlook from adding extra spacing to tables. */
            table,
            td {
                mso-table-lspace: 0pt !important;
                mso-table-rspace: 0pt !important;
            }
    
            /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
            table {
                border-spacing: 0 !important;
                border-collapse: collapse !important;
                table-layout: fixed !important;
                margin: 0 auto !important;
            }
            table table table {
                table-layout: auto;
            }
    
            /* What it does: Uses a better rendering method when resizing images in IE. */
            img {
                -ms-interpolation-mode:bicubic;
            }
    
            /* What it does: A work-around for email clients meddling in triggered links. */
            *[x-apple-data-detectors],  /* iOS */
            .x-gmail-data-detectors,    /* Gmail */
            .x-gmail-data-detectors *,
            .aBn {
                border-bottom: 0 !important;
                cursor: default !important;
                color: inherit !important;
                text-decoration: none !important;
                font-size: inherit !important;
                font-family: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
            }
    
            /* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */
            .a6S {
                display: none !important;
                opacity: 0.01 !important;
            }
            /* If the above doesn't work, add a .g-img class to any image in question. */
            img.g-img + div {
                display:none !important;
               }
    
            /* What it does: Prevents underlining the button text in Windows 10 */
            .button-link {
                text-decoration: none !important;
            }
    
            /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
            /* Create one of these media queries for each additional viewport size you'd like to fix */
            /* Thanks to Eric Lepetit @ericlepetitsf) for help troubleshooting */
            @media only screen and (min-device-width: 375px) and (max-device-width: 413px) { /* iPhone 6 and 6+ */
                .email-container {
                    min-width: 375px !important;
                }
            }
            .hero_bg{
                background-color: #330033;
                background-image: url("https://reverieinc.com/prabandhak/assets/header.jpg");
            }
        </style>
    
        <!-- Progressive Enhancements -->
        <style>
    
            /* What it does: Hover styles for buttons */
            .button-td,
            .button-a {
                transition: all 100ms ease-in;
            }
            .button-td:hover,
            .button-a:hover {
                background: #555555 !important;
                border-color: #555555 !important;
            }
    
            /* Media Queries */
            @media screen and (max-width: 480px) {
    
                /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
                .fluid {
                    width: 100% !important;
                    max-width: 100% !important;
                    height: auto !important;
                    margin-left: auto !important;
                    margin-right: auto !important;
                }
    
                /* What it does: Forces table cells into full-width rows. */
                .stack-column,
                .stack-column-center {
                    display: block !important;
                    width: 100% !important;
                    max-width: 100% !important;
                    direction: ltr !important;
                }
                /* And center justify these ones. */
                .stack-column-center {
                    text-align: center !important;
                }
    
                /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
                .center-on-narrow {
                    text-align: center !important;
                    display: block !important;
                    margin-left: auto !important;
                    margin-right: auto !important;
                    float: none !important;
                }
                table.center-on-narrow {
                    display: inline-block !important;
                }
    
                /* What it does: Adjust typography on small screens to improve readability */
                .email-container p {
                    font-size: 17px !important;
                    line-height: 22px !important;
                }
            }
    
        </style>
    
        <!-- What it does: Makes background images in 72ppi Outlook render at correct size. -->
        <!--[if gte mso 9]>
        <xml>
            <o:OfficeDocumentSettings>
                <o:AllowPNG/>
                <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
    
    </head>
    <body width="100%" bgcolor="#F1F1F1" style="margin: 0; mso-line-height-rule: exactly;">
        <center style="width: 100%; background: #F1F1F1; text-align: left;">
    
            <!-- Visually Hidden Preheader Text : BEGIN -->
            <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
                Translate for India and earn while sipping coffee at home !!! 
            </div>
            <!-- Visually Hidden Preheader Text : END -->
    
            <!--
                Set the email width. Defined in two places:
                1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 680px.
                2. MSO tags for Desktop Windows Outlook enforce a 680px width.
                Note: The Fluid and Responsive templates have a different width (600px). The hybrid grid is more "fragile", and I've found that 680px is a good width. Change with caution.
            -->
            <div style="max-width: 680px; margin: auto;" class="email-container">
                <!--[if mso]>
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="680" align="center">
                <tr>
                <td>
                <![endif]-->
    
                <!-- Email Body : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;" class="email-container">
    
    
    
    
                    <!-- HERO : BEGIN -->
                    <tr>
                        <!-- Bulletproof Background Images c/o https://backgrounds.cm -->
                        <td class="hero_bg" bgcolor="#222222" align="center" valign="top" style="text-align: center; background-position: center center !important; background-size: cover !important;">
                            <!--[if gte mso 9]>
                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:680px; height:380px; background-position: center center !important;">
                            <v:fill type="tile" src="background.png" color="#222222" />
                            <v:textbox inset="0,0,0,0">
                            <![endif]-->
                            <div>
                                <!--[if mso]>
                                <table role="presentation" border="0" cellspacing="0" cellpadding="0" align="center" width="500">
                                <tr>
                                <td align="center" valign="middle" width="500">
                                <![endif]-->
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:500px; margin: auto;">
    
                                    <tr>
                                        <td height="20" style="font-size:20px; line-height:20px;">&nbsp;</td>
                                    </tr>
    
                                    <tr>
                                      <td align="center" valign="middle">
                                        
                                      <table>
                                         <tr>
                                             <td valign="top" style="text-align: center; padding: 60px 0 10px 0px;">
                                                <img src="https://reverieinc.com/prabandhak/assets/logo.png" alt="" width="60px">
                                                 
                                                 <h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 26px; line-height: 36px; color: #ffffff; font-weight: bold;">PRABANDHAK</h1>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td valign="top" style="text-align: center; padding: 10px 20px 15px 20px; font-family: 'Montserrat',sans-serif; font-size: 20px; line-height: 20px; color: #fff; font-weight:300;">
                                                 <p>First ever AI powered translation marketplace for Indian Languages</p>
                                                 <p style="font-weight:400;font-size: 13px;">#IndianizeTheInternet #madeinindia</p>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td valign="top" align="center" style="text-align: center; padding: 15px 0px 60px 0px;">
    
                                                 <!-- Button : BEGIN -->
                                                 <center>
                                                 <table role="presentation" align="center" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow" style="text-align: center;">
                                                     
                                                 </table>
                                                 </center>
                                                 <!-- Button : END -->
    
                                             </td>
                                         </tr> 
                                      </table>
    
                                      </td>
                                    </tr>
                                
                                    <tr>
                                        <td height="20" style="font-size:20px; line-height:20px;">&nbsp;</td>
                                    </tr>
    
                                </table>
                                <!--[if mso]>
                                </td>
                                </tr>
                                </table>
                                <![endif]-->
                            </div>
                            <!--[if gte mso 9]>
                            </v:textbox>
                            </v:rect>
                            <![endif]-->
                        </td>
                    </tr>
                    <!-- HERO : END -->
    
                    <!-- INTRO : BEGIN -->
                    <tr>
                        <td bgcolor="#ffffff">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td style="padding: 40px 40px 20px 40px; text-align: left;">
                                        <h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 16px; line-height: 26px; color: #333333; font-weight: bold;">Greetings,</h1>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0px 40px 20px 40px; font-family: 'Montserrat', sans-serif; font-size: 15px; line-height: 26px; color: #555555; text-align: left; font-weight:300;">
                                        <p style="margin: 0;">!!!EEETTT!!!</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0px 40px 20px 40px; font-family: 'Montserrat', sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: left; font-weight:400;">
                                        <p style="margin: 0;">Yours sincerely,</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0px 40px 20px 40px; font-family: 'Montserrat', sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: left; font-weight:600;">
                                        <p style="margin: 0;">Team Prabandhak</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- INTRO : END -->
    
                    <!-- AGENDA : BEGIN -->
                  <!--   <tr>
                        <td bgcolor="#f7fafc">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td style="padding: 40px 40px 20px 40px; text-align: left;">
                                        <h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 20px; line-height: 26px; color: #333333; font-weight: bold;">AGENDA</h1>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0px 40px 35px 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
                                        <p style="margin: 0;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat.</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0px 40px 20px 40px;">
                                        
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" style="border:1px solid #dddddd; border-left:3px solid #26a4d3;">
                                            <tr>
                                                <td align="" style="padding: 20px 20px 0px 20px; font-family: 'Montserrat', sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
                                                    <h3 style="margin:0;">
                                                        Agenda 1).
                                                    </h3>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="" style="padding: 20px 20px 20px 20px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
                                                    <p style="margin:0;">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                        consequat.
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>
    
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0px 40px 40px 40px;">
                                        
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" style="border:1px solid #dddddd; border-left:3px solid #26a4d3;">
                                            <tr>
                                                <td align="" style="padding: 20px 20px 0px 20px; font-family: 'Montserrat', sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
                                                    <h3 style="margin:0;">
                                                        Agenda 2).
                                                    </h3>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="" style="padding: 20px 20px 20px 20px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
                                                    <p style="margin:0;">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                        consequat.
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>
    
                                    </td>
                                </tr>
    
                            </table>
                        </td>
                    </tr> -->
                    <!-- AGENDA : END -->
    
                    <!-- CTA : BEGIN -->
                    <tr>
                        <td class="hero_bg">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td style="padding: 40px 40px 5px 40px; text-align: center;">
                                        <h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 20px; line-height: 24px; color: #ffffff; font-weight: 300;">Language is a social construct<br> It's time the society pays back</h1>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle" align="center" style="text-align: center; padding: 40px 20px 40px 20px;">
    
                                        <!-- Button : BEGIN --
                                        <table role="presentation" align="center" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow">
                                            <tr>
                                                <td style="border-radius: 50px; background: #ffffff; text-align: center;" class="button-td">
                                                    <a href="!!!TEMPLATE_URL!!!" style="background: #ffffff; border: 15px solid #ffffff; font-family: 'Montserrat', sans-serif; font-size: 14px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 50px; font-weight: bold;" class="button-a">
                                                        <span style="color:#26a4d3;" class="button-link">&nbsp;&nbsp;&nbsp;&nbsp;Let's Go&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                        !-- Button : END -->
    
                                    </td>
                                </tr>
    
                            </table>
                        </td>
                    </tr>
                    <!-- CTA : END -->
    
                    <!-- SOCIAL : BEGIN -->
                    <!-- <tr>
                        <td bgcolor="#292828">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td style="padding: 30px 30px; text-align: center;">
                                        
                                        <table align="center" style="text-align: center;">
                                            <tr>
                                                <td>
                                                    <i style="font-size:14px; color:#fff;"class="fab fa-twitter"></i>
                                                </td>
                                                <td width="10">&nbsp;</td>
                                                <td>
                                                    <i style="font-size:14px; color:#fff;"class="fab fa-facebook"></i>
                                                </td>
                                                <td width="10">&nbsp;</td>
                                                <td>
                                                    <i style="font-size:14px; color:#fff;"class="fab fa-linkedin"></i>
                                                </td>
                                            </tr>
                                        </table>
    
                                    </td>
                                </tr>
    
                            </table>
                        </td>
                    </tr> -->
                    <!-- SOCIAL : END -->
    
                    <!-- FOOTER : BEGIN -->
                    <tr>
                        <td bgcolor="#ffffff">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                
                                <tr>
                                    <td style="padding:30px 40px 24px 40px; font-family: sans-serif; font-size: 12px; line-height: 18px; color: #666666; text-align: center; font-weight:normal;">
                                        <p style="margin: 0;">Copyright &copy; 2018-2019 <b>Prabandhak Inc.</b>, All Rights Reserved.</p>
                                    </td>
                                </tr>
    
                            </table>
                        </td>
                    </tr>
                    <!-- FOOTER : END -->
    
                </table>
                <!-- Email Body : END -->
    
                <!--[if mso]>
                </td>
                </tr>
                </table>
                <![endif]-->
            </div>
    
        </center>
    </body>
    </html>`,
    freelancerinvite : `<!-- THIS EMAIL WAS BUILT AND TESTED WITH LITMUS http://litmus.com -->
	<!-- IT WAS RELEASED UNDER THE MIT LICENSE https://opensource.org/licenses/MIT -->
	<!-- QUESTIONS? TWEET US @LITMUSAPP -->
	<!DOCTYPE html>
	<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
	<head>
		<meta charset="utf-8"> <!-- utf-8 works for most cases -->
		<meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
		<meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
	  <!--   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous"> -->
		<script defer src="https://use.fontawesome.com/releases/v5.0.12/js/all.js" integrity="sha384-Voup2lBiiyZYkRto2XWqbzxHXwzcm4A5RfdfG6466bu5LqjwwrjXCMBQBLMWh7qR" crossorigin="anonymous"></script>
		<title>Event - [Plain HTML]</title> <!-- The title tag shows in email notifications, like Android 4.4. -->
	
		<!-- Web Font / @font-face : BEGIN -->
		<!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->
	
		<!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
		<!--[if mso]>
			<style>
				* {
					font-family: Arial, sans-serif !important;
				}
			</style>
		<![endif]-->
	
		<!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
		<!--[if !mso]><!-->
			<!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet"> -->
		<!--<![endif]-->
	
		<!-- Web Font / @font-face : END -->
	
		<!-- CSS Reset -->
		<style>
	
			/* What it does: Remove spaces around the email design added by some email clients. */
			/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
			html,
			body {
				margin: 0 auto !important;
				padding: 0 !important;
				height: 100% !important;
				width: 100% !important;
	
			}
			
			/* What it does: Stops email clients resizing small text. */
			* {
				-ms-text-size-adjust: 100%;
				-webkit-text-size-adjust: 100%;
				font-family: monospace !important;
			}
	
			/* What it does: Centers email on Android 4.4 */
			div[style*="margin: 16px 0"] {
				margin:0 !important;
			}
	
			/* What it does: Stops Outlook from adding extra spacing to tables. */
			table,
			td {
				mso-table-lspace: 0pt !important;
				mso-table-rspace: 0pt !important;
			}
	
			/* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
			table {
				border-spacing: 0 !important;
				border-collapse: collapse !important;
				table-layout: fixed !important;
				margin: 0 auto !important;
			}
			table table table {
				table-layout: auto;
			}
	
			/* What it does: Uses a better rendering method when resizing images in IE. */
			img {
				-ms-interpolation-mode:bicubic;
			}
	
			/* What it does: A work-around for email clients meddling in triggered links. */
			*[x-apple-data-detectors],	/* iOS */
			.x-gmail-data-detectors, 	/* Gmail */
			.x-gmail-data-detectors *,
			.aBn {
				border-bottom: 0 !important;
				cursor: default !important;
				color: inherit !important;
				text-decoration: none !important;
				font-size: inherit !important;
				font-family: inherit !important;
				font-weight: inherit !important;
				line-height: inherit !important;
			}
	
			/* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */
			.a6S {
				display: none !important;
				opacity: 0.01 !important;
			}
			/* If the above doesn't work, add a .g-img class to any image in question. */
			img.g-img + div {
				display:none !important;
			   }
	
			/* What it does: Prevents underlining the button text in Windows 10 */
			.button-link {
				text-decoration: none !important;
			}
	
			/* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
			/* Create one of these media queries for each additional viewport size you'd like to fix */
			/* Thanks to Eric Lepetit @ericlepetitsf) for help troubleshooting */
			@media only screen and (min-device-width: 375px) and (max-device-width: 413px) { /* iPhone 6 and 6+ */
				.email-container {
					min-width: 375px !important;
				}
			}
			.hero_bg{
				background-color: #330033;
				background-image: url("https://reverieinc.com/prabandhak/assets/header.jpg");
			}
		</style>
	
		<!-- Progressive Enhancements -->
		<style>
	
			/* What it does: Hover styles for buttons */
			.button-td,
			.button-a {
				transition: all 100ms ease-in;
			}
			.button-td:hover,
			.button-a:hover {
				background: #555555 !important;
				border-color: #555555 !important;
			}
	
			/* Media Queries */
			@media screen and (max-width: 480px) {
	
				/* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
				.fluid {
					width: 100% !important;
					max-width: 100% !important;
					height: auto !important;
					margin-left: auto !important;
					margin-right: auto !important;
				}
	
				/* What it does: Forces table cells into full-width rows. */
				.stack-column,
				.stack-column-center {
					display: block !important;
					width: 100% !important;
					max-width: 100% !important;
					direction: ltr !important;
				}
				/* And center justify these ones. */
				.stack-column-center {
					text-align: center !important;
				}
	
				/* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
				.center-on-narrow {
					text-align: center !important;
					display: block !important;
					margin-left: auto !important;
					margin-right: auto !important;
					float: none !important;
				}
				table.center-on-narrow {
					display: inline-block !important;
				}
	
				/* What it does: Adjust typography on small screens to improve readability */
				.email-container p {
					font-size: 17px !important;
					line-height: 22px !important;
				}
			}
	
		</style>
	
		<!-- What it does: Makes background images in 72ppi Outlook render at correct size. -->
		<!--[if gte mso 9]>
		<xml>
			<o:OfficeDocumentSettings>
				<o:AllowPNG/>
				<o:PixelsPerInch>96</o:PixelsPerInch>
			</o:OfficeDocumentSettings>
		</xml>
		<![endif]-->
	
	</head>
	<body width="100%" bgcolor="#F1F1F1" style="margin: 0; mso-line-height-rule: exactly;">
		<center style="width: 100%; background: #F1F1F1; text-align: left;">
	
			<!-- Visually Hidden Preheader Text : BEGIN -->
			<div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
				Translate for India and earn while sipping coffee at home !!! 
			</div>
			<!-- Visually Hidden Preheader Text : END -->
	
			<!--
				Set the email width. Defined in two places:
				1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 680px.
				2. MSO tags for Desktop Windows Outlook enforce a 680px width.
				Note: The Fluid and Responsive templates have a different width (600px). The hybrid grid is more "fragile", and I've found that 680px is a good width. Change with caution.
			-->
			<div style="max-width: 680px; margin: auto;" class="email-container">
				<!--[if mso]>
				<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="680" align="center">
				<tr>
				<td>
				<![endif]-->
	
				<!-- Email Body : BEGIN -->
				<table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;" class="email-container">
	
	
	
	
					<!-- HERO : BEGIN -->
					<tr>
						<!-- Bulletproof Background Images c/o https://backgrounds.cm -->
						<td class="hero_bg" bgcolor="#222222" align="center" valign="top" style="text-align: center; background-position: center center !important; background-size: cover !important;">
							<!--[if gte mso 9]>
							<v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:680px; height:380px; background-position: center center !important;">
							<v:fill type="tile" src="background.png" color="#222222" />
							<v:textbox inset="0,0,0,0">
							<![endif]-->
							<div>
								<!--[if mso]>
								<table role="presentation" border="0" cellspacing="0" cellpadding="0" align="center" width="500">
								<tr>
								<td align="center" valign="middle" width="500">
								<![endif]-->
								<table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:500px; margin: auto;">
	
									<tr>
										<td height="20" style="font-size:20px; line-height:20px;">&nbsp;</td>
									</tr>
	
									<tr>
									  <td align="center" valign="middle">
										
									  <table>
										 <tr>
											 <td valign="top" style="text-align: center; padding: 60px 0 10px 0px;">
												<img src="https://reverieinc.com/prabandhak/assets/logo.png" alt="" width="60px">
												 
												 <h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 26px; line-height: 36px; color: #ffffff; font-weight: bold;">PRABANDHAK</h1>
											 </td>
										 </tr>
										 <tr>
											 <td valign="top" style="text-align: center; padding: 10px 20px 15px 20px; font-family: 'Montserrat',sans-serif; font-size: 20px; line-height: 20px; color: #fff; font-weight:300;">
												 <p>First ever AI powered translation marketplace for Indian Languages</p>
												 <p style="font-weight:400;font-size: 13px;">#IndianizeTheInternet #madeinindia</p>
											 </td>
										 </tr>
										 <tr>
											 <td valign="top" align="center" style="text-align: center; padding: 15px 0px 60px 0px;">
	
												 <!-- Button : BEGIN -->
												 <center>
												 <table role="presentation" align="center" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow" style="text-align: center;">
													 
												 </table>
												 </center>
												 <!-- Button : END -->
	
											 </td>
										 </tr> 
									  </table>
	
									  </td>
									</tr>
								
									<tr>
										<td height="20" style="font-size:20px; line-height:20px;">&nbsp;</td>
									</tr>
	
								</table>
								<!--[if mso]>
								</td>
								</tr>
								</table>
								<![endif]-->
							</div>
							<!--[if gte mso 9]>
							</v:textbox>
							</v:rect>
							<![endif]-->
						</td>
					</tr>
					<!-- HERO : END -->
	
					<!-- INTRO : BEGIN -->
					<tr>
						<td bgcolor="#ffffff">
							<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td style="padding: 40px 40px 20px 40px; text-align: left;">
										<h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 20px; line-height: 26px; color: #333333; font-weight: bold;">Greetings,</h1>
									</td>
								</tr>
								<tr>
									<td style="padding: 0px 40px 20px 40px; font-family: 'Montserrat', sans-serif; font-size: 15px; line-height: 26px; color: #555555; text-align: left; font-weight:300;">
										<p style="margin: 0;">Anyway this a test mail. We will be sending you something more substantial next time. Actually we need some content for this mail.</p>
									</td>
								</tr>
								<tr>
									<td style="padding: 0px 40px 20px 40px; font-family: 'Montserrat', sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: left; font-weight:400;">
										<p style="margin: 0;">Yours sincerely,</p>
									</td>
								</tr>
	
								<tr>
									<td align="left" style="padding: 0px 40px 40px 40px;">
	
										<table width="180" align="left">
											<tr>
											  
											  <td width="110">
												
												<table width="" cellpadding="0" cellspacing="0" border="0">
												  
												  <tr>
													<td align="left" style="font-family: 'Montserrat', sans-serif; font-size:14px; line-height:20px; color:#666666;" class="body-text">
													  <p style="font-family: 'Montserrat',sans-serif; font-size:14px; line-height:20px; color:#666666; padding:0; margin:0;" class="body-text">Team Prabandhak</p>    
													</td>               
												  </tr>                            
												</table>
	
											  </td>                        
											</tr>
										</table>
	
									</td>
								</tr>
	
							</table>
						</td>
					</tr>
					<!-- INTRO : END -->
	
					<!-- AGENDA : BEGIN -->
				  <!--   <tr>
						<td bgcolor="#f7fafc">
							<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td style="padding: 40px 40px 20px 40px; text-align: left;">
										<h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 20px; line-height: 26px; color: #333333; font-weight: bold;">AGENDA</h1>
									</td>
								</tr>
								<tr>
									<td style="padding: 0px 40px 35px 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
										<p style="margin: 0;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
										quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
										consequat.</p>
									</td>
								</tr>
								<tr>
									<td style="padding: 0px 40px 20px 40px;">
										
										<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" style="border:1px solid #dddddd; border-left:3px solid #26a4d3;">
											<tr>
												<td align="" style="padding: 20px 20px 0px 20px; font-family: 'Montserrat', sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
													<h3 style="margin:0;">
														Agenda 1).
													</h3>
												</td>
											</tr>
											<tr>
												<td align="" style="padding: 20px 20px 20px 20px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
													<p style="margin:0;">
														Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
														tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
														quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
														consequat.
													</p>
												</td>
											</tr>
										</table>
	
									</td>
								</tr>
								<tr>
									<td style="padding: 0px 40px 40px 40px;">
										
										<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" style="border:1px solid #dddddd; border-left:3px solid #26a4d3;">
											<tr>
												<td align="" style="padding: 20px 20px 0px 20px; font-family: 'Montserrat', sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
													<h3 style="margin:0;">
														Agenda 2).
													</h3>
												</td>
											</tr>
											<tr>
												<td align="" style="padding: 20px 20px 20px 20px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
													<p style="margin:0;">
														Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
														tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
														quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
														consequat.
													</p>
												</td>
											</tr>
										</table>
	
									</td>
								</tr>
	
							</table>
						</td>
					</tr> -->
					<!-- AGENDA : END -->
	
					<!-- CTA : BEGIN -->
					<tr>
						<td class="hero_bg">
							<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td style="padding: 40px 40px 5px 40px; text-align: center;">
										<h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 20px; line-height: 24px; color: #ffffff; font-weight: 300;">Language is a social construct<br> It's time the society pays back</h1>
									</td>
								</tr>
								<tr>
									<td valign="middle" align="center" style="text-align: center; padding: 40px 20px 40px 20px;">
	
										<!-- Button : BEGIN -->
										<table role="presentation" align="center" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow">
											<tr>
												<td style="border-radius: 50px; background: #ffffff; text-align: center;" class="button-td">
													<a href="!!!TEMPLATE_URL!!!" style="background: #ffffff; border: 15px solid #ffffff; font-family: 'Montserrat', sans-serif; font-size: 14px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 50px; font-weight: bold;" class="button-a">
														<span style="color:#26a4d3;" class="button-link">&nbsp;&nbsp;&nbsp;&nbsp;Let's Go&nbsp;&nbsp;&nbsp;&nbsp;</span>
													</a>
												</td>
											</tr>
										</table>
										<!-- Button : END -->
	
									</td>
								</tr>
	
							</table>
						</td>
					</tr>
					<!-- CTA : END -->
	
					<!-- SOCIAL : BEGIN -->
					<!-- <tr>
						<td bgcolor="#292828">
							<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td style="padding: 30px 30px; text-align: center;">
										
										<table align="center" style="text-align: center;">
											<tr>
												<td>
													<i style="font-size:14px; color:#fff;"class="fab fa-twitter"></i>
												</td>
												<td width="10">&nbsp;</td>
												<td>
													<i style="font-size:14px; color:#fff;"class="fab fa-facebook"></i>
												</td>
												<td width="10">&nbsp;</td>
												<td>
													<i style="font-size:14px; color:#fff;"class="fab fa-linkedin"></i>
												</td>
											</tr>
										</table>
	
									</td>
								</tr>
	
							</table>
						</td>
					</tr> -->
					<!-- SOCIAL : END -->
	
					<!-- FOOTER : BEGIN -->
					<tr>
						<td bgcolor="#ffffff">
							<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
								
								<tr>
									<td style="padding:30px 40px 24px 40px; font-family: sans-serif; font-size: 12px; line-height: 18px; color: #666666; text-align: center; font-weight:normal;">
										<p style="margin: 0;">Copyright &copy; 2018-2019 <b>Prabandhak Inc.</b>, All Rights Reserved.</p>
									</td>
								</tr>
	
							</table>
						</td>
					</tr>
					<!-- FOOTER : END -->
	
				</table>
				<!-- Email Body : END -->
	
				<!--[if mso]>
				</td>
				</tr>
				</table>
				<![endif]-->
			</div>
	
		</center>
	</body>
	</html>
	`,
	partnerTeamInvite: `<!-- THIS EMAIL WAS BUILT AND TESTED WITH LITMUS http://litmus.com -->
	<!-- IT WAS RELEASED UNDER THE MIT LICENSE https://opensource.org/licenses/MIT -->
	<!-- QUESTIONS? TWEET US @LITMUSAPP -->
	<!DOCTYPE html>
	<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
	<head>
		<meta charset="utf-8"> <!-- utf-8 works for most cases -->
		<meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
		<meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
	  <!--   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous"> -->
		<script defer src="https://use.fontawesome.com/releases/v5.0.12/js/all.js" integrity="sha384-Voup2lBiiyZYkRto2XWqbzxHXwzcm4A5RfdfG6466bu5LqjwwrjXCMBQBLMWh7qR" crossorigin="anonymous"></script>
		<title>Event - [Plain HTML]</title> <!-- The title tag shows in email notifications, like Android 4.4. -->
	
		<!-- Web Font / @font-face : BEGIN -->
		<!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->
	
		<!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
		<!--[if mso]>
			<style>
				* {
					font-family: Arial, sans-serif !important;
				}
			</style>
		<![endif]-->
	
		<!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
		<!--[if !mso]><!-->
			<!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet"> -->
		<!--<![endif]-->
	
		<!-- Web Font / @font-face : END -->
	
		<!-- CSS Reset -->
		<style>
	
			/* What it does: Remove spaces around the email design added by some email clients. */
			/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
			html,
			body {
				margin: 0 auto !important;
				padding: 0 !important;
				height: 100% !important;
				width: 100% !important;
	
			}
			
			/* What it does: Stops email clients resizing small text. */
			* {
				-ms-text-size-adjust: 100%;
				-webkit-text-size-adjust: 100%;
				font-family: monospace !important;
			}
	
			/* What it does: Centers email on Android 4.4 */
			div[style*="margin: 16px 0"] {
				margin:0 !important;
			}
	
			/* What it does: Stops Outlook from adding extra spacing to tables. */
			table,
			td {
				mso-table-lspace: 0pt !important;
				mso-table-rspace: 0pt !important;
			}
	
			/* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
			table {
				border-spacing: 0 !important;
				border-collapse: collapse !important;
				table-layout: fixed !important;
				margin: 0 auto !important;
			}
			table table table {
				table-layout: auto;
			}
	
			/* What it does: Uses a better rendering method when resizing images in IE. */
			img {
				-ms-interpolation-mode:bicubic;
			}
	
			/* What it does: A work-around for email clients meddling in triggered links. */
			*[x-apple-data-detectors],	/* iOS */
			.x-gmail-data-detectors, 	/* Gmail */
			.x-gmail-data-detectors *,
			.aBn {
				border-bottom: 0 !important;
				cursor: default !important;
				color: inherit !important;
				text-decoration: none !important;
				font-size: inherit !important;
				font-family: inherit !important;
				font-weight: inherit !important;
				line-height: inherit !important;
			}
	
			/* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */
			.a6S {
				display: none !important;
				opacity: 0.01 !important;
			}
			/* If the above doesn't work, add a .g-img class to any image in question. */
			img.g-img + div {
				display:none !important;
			   }
	
			/* What it does: Prevents underlining the button text in Windows 10 */
			.button-link {
				text-decoration: none !important;
			}
	
			/* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
			/* Create one of these media queries for each additional viewport size you'd like to fix */
			/* Thanks to Eric Lepetit @ericlepetitsf) for help troubleshooting */
			@media only screen and (min-device-width: 375px) and (max-device-width: 413px) { /* iPhone 6 and 6+ */
				.email-container {
					min-width: 375px !important;
				}
			}
			.hero_bg{
				background-color: #330033;
				background-image: url("https://reverieinc.com/prabandhak/assets/header.jpg");
			}
		</style>
	
		<!-- Progressive Enhancements -->
		<style>
	
			/* What it does: Hover styles for buttons */
			.button-td,
			.button-a {
				transition: all 100ms ease-in;
			}
			.button-td:hover,
			.button-a:hover {
				background: #555555 !important;
				border-color: #555555 !important;
			}
	
			/* Media Queries */
			@media screen and (max-width: 480px) {
	
				/* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
				.fluid {
					width: 100% !important;
					max-width: 100% !important;
					height: auto !important;
					margin-left: auto !important;
					margin-right: auto !important;
				}
	
				/* What it does: Forces table cells into full-width rows. */
				.stack-column,
				.stack-column-center {
					display: block !important;
					width: 100% !important;
					max-width: 100% !important;
					direction: ltr !important;
				}
				/* And center justify these ones. */
				.stack-column-center {
					text-align: center !important;
				}
	
				/* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
				.center-on-narrow {
					text-align: center !important;
					display: block !important;
					margin-left: auto !important;
					margin-right: auto !important;
					float: none !important;
				}
				table.center-on-narrow {
					display: inline-block !important;
				}
	
				/* What it does: Adjust typography on small screens to improve readability */
				.email-container p {
					font-size: 17px !important;
					line-height: 22px !important;
				}
			}
	
		</style>
	
		<!-- What it does: Makes background images in 72ppi Outlook render at correct size. -->
		<!--[if gte mso 9]>
		<xml>
			<o:OfficeDocumentSettings>
				<o:AllowPNG/>
				<o:PixelsPerInch>96</o:PixelsPerInch>
			</o:OfficeDocumentSettings>
		</xml>
		<![endif]-->
	
	</head>
	<body width="100%" bgcolor="#F1F1F1" style="margin: 0; mso-line-height-rule: exactly;">
		<center style="width: 100%; background: #F1F1F1; text-align: left;">
	
			<!-- Visually Hidden Preheader Text : BEGIN -->
			<div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
				Translate for India and earn while sipping coffee at home !!! 
			</div>
			<!-- Visually Hidden Preheader Text : END -->
	
			<!--
				Set the email width. Defined in two places:
				1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 680px.
				2. MSO tags for Desktop Windows Outlook enforce a 680px width.
				Note: The Fluid and Responsive templates have a different width (600px). The hybrid grid is more "fragile", and I've found that 680px is a good width. Change with caution.
			-->
			<div style="max-width: 680px; margin: auto;" class="email-container">
				<!--[if mso]>
				<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="680" align="center">
				<tr>
				<td>
				<![endif]-->
	
				<!-- Email Body : BEGIN -->
				<table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;" class="email-container">
	
	
	
	
					<!-- HERO : BEGIN -->
					<tr>
						<!-- Bulletproof Background Images c/o https://backgrounds.cm -->
						<td class="hero_bg" bgcolor="#222222" align="center" valign="top" style="text-align: center; background-position: center center !important; background-size: cover !important;">
							<!--[if gte mso 9]>
							<v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:680px; height:380px; background-position: center center !important;">
							<v:fill type="tile" src="background.png" color="#222222" />
							<v:textbox inset="0,0,0,0">
							<![endif]-->
							<div>
								<!--[if mso]>
								<table role="presentation" border="0" cellspacing="0" cellpadding="0" align="center" width="500">
								<tr>
								<td align="center" valign="middle" width="500">
								<![endif]-->
								<table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:500px; margin: auto;">
	
									<tr>
										<td height="20" style="font-size:20px; line-height:20px;">&nbsp;</td>
									</tr>
	
									<tr>
									  <td align="center" valign="middle">
										
									  <table>
										 <tr>
											 <td valign="top" style="text-align: center; padding: 60px 0 10px 0px;">
												<img src="https://reverieinc.com/prabandhak/assets/logo.png" alt="" width="60px">
												 
												 <h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 26px; line-height: 36px; color: #ffffff; font-weight: bold;">PRABANDHAK</h1>
											 </td>
										 </tr>
										 <tr>
											 <td valign="top" style="text-align: center; padding: 10px 20px 15px 20px; font-family: 'Montserrat',sans-serif; font-size: 20px; line-height: 20px; color: #fff; font-weight:300;">
												 <p>First ever AI powered translation marketplace for Indian Languages</p>
												 <p style="font-weight:400;font-size: 13px;">#IndianizeTheInternet #madeinindia</p>
											 </td>
										 </tr>
										 <tr>
											 <td valign="top" align="center" style="text-align: center; padding: 15px 0px 60px 0px;">
	
												 <!-- Button : BEGIN -->
												 <center>
												 <table role="presentation" align="center" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow" style="text-align: center;">
													 
												 </table>
												 </center>
												 <!-- Button : END -->
	
											 </td>
										 </tr> 
									  </table>
	
									  </td>
									</tr>
								
									<tr>
										<td height="20" style="font-size:20px; line-height:20px;">&nbsp;</td>
									</tr>
	
								</table>
								<!--[if mso]>
								</td>
								</tr>
								</table>
								<![endif]-->
							</div>
							<!--[if gte mso 9]>
							</v:textbox>
							</v:rect>
							<![endif]-->
						</td>
					</tr>
					<!-- HERO : END -->
	
					<!-- INTRO : BEGIN -->
					<tr>
						<td bgcolor="#ffffff">
							<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td style="padding: 40px 40px 20px 40px; text-align: left;">
										<h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 20px; line-height: 26px; color: #333333; font-weight: bold;">Greetings,</h1>
									</td>
								</tr>
								
								<tr>
									<td style="padding: 0px 40px 20px 40px; font-family: 'Montserrat', sans-serif; font-size: 15px; line-height: 26px; color: #555555; text-align: left; font-weight:300;">
										<p style="margin: 0;">!!!SENDER!!!, from !!!ORGANISATION!!! has sent you an invitation to join his team as a translator. You will be part of a translation team, now you can work from anywhere and anytime, with just an internet connection and a laptop. Click on the "Let's Go" button below to get started.</p>
									</td>
								</tr>
								<tr>
									<td style="padding: 0px 40px 20px 40px; font-family: 'Montserrat', sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: left; font-weight:400;">
										<p style="margin: 0;">Yours sincerely,</p>
									</td>
								</tr>
	
								<tr>
									<td align="left" style="padding: 0px 40px 40px 40px;">
	
										<table width="180" align="left">
											<tr>
											  
											  <td width="110">
												
												<table width="" cellpadding="0" cellspacing="0" border="0">
												 
												  <tr>
													<td align="left" style="font-family: 'Montserrat', sans-serif; font-size:14px; line-height:20px; color:#666666;" class="body-text">
													  <p style="font-family: 'Montserrat',sans-serif; font-size:14px; line-height:20px; color:#666666; padding:0; margin:0;" class="body-text">Team Prabandhak</p>    
													</td>               
												  </tr>                            
												</table>
	
											  </td>                        
											</tr>
										</table>
	
									</td>
								</tr>
	
							</table>
						</td>
					</tr>
					<!-- INTRO : END -->
	
					<!-- AGENDA : BEGIN -->
				  <!--   <tr>
						<td bgcolor="#f7fafc">
							<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td style="padding: 40px 40px 20px 40px; text-align: left;">
										<h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 20px; line-height: 26px; color: #333333; font-weight: bold;">AGENDA</h1>
									</td>
								</tr>
								<tr>
									<td style="padding: 0px 40px 35px 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
										<p style="margin: 0;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
										quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
										consequat.</p>
									</td>
								</tr>
								<tr>
									<td style="padding: 0px 40px 20px 40px;">
										
										<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" style="border:1px solid #dddddd; border-left:3px solid #26a4d3;">
											<tr>
												<td align="" style="padding: 20px 20px 0px 20px; font-family: 'Montserrat', sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
													<h3 style="margin:0;">
														Agenda 1).
													</h3>
												</td>
											</tr>
											<tr>
												<td align="" style="padding: 20px 20px 20px 20px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
													<p style="margin:0;">
														Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
														tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
														quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
														consequat.
													</p>
												</td>
											</tr>
										</table>
	
									</td>
								</tr>
								<tr>
									<td style="padding: 0px 40px 40px 40px;">
										
										<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" style="border:1px solid #dddddd; border-left:3px solid #26a4d3;">
											<tr>
												<td align="" style="padding: 20px 20px 0px 20px; font-family: 'Montserrat', sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
													<h3 style="margin:0;">
														Agenda 2).
													</h3>
												</td>
											</tr>
											<tr>
												<td align="" style="padding: 20px 20px 20px 20px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
													<p style="margin:0;">
														Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
														tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
														quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
														consequat.
													</p>
												</td>
											</tr>
										</table>
	
									</td>
								</tr>
	
							</table>
						</td>
					</tr> -->
					<!-- AGENDA : END -->
	
					<!-- CTA : BEGIN -->
					<tr>
						<td class="hero_bg">
							<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td style="padding: 40px 40px 5px 40px; text-align: center;">
										<h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 20px; line-height: 24px; color: #ffffff; font-weight: 300;">Language is a social construct<br> It's time the society pays back</h1>
									</td>
								</tr>
								<tr>
									<td valign="middle" align="center" style="text-align: center; padding: 40px 20px 40px 20px;">
	
										<!-- Button : BEGIN -->
										<table role="presentation" align="center" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow">
											<tr>
												<td style="border-radius: 50px; background: #ffffff; text-align: center;" class="button-td">
													<a href="!!!TEMPLATE_URL!!!" style="background: #ffffff; border: 15px solid #ffffff; font-family: 'Montserrat', sans-serif; font-size: 14px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 50px; font-weight: bold;" class="button-a">
														<span style="color:#26a4d3;" class="button-link">&nbsp;&nbsp;&nbsp;&nbsp;Let's Go&nbsp;&nbsp;&nbsp;&nbsp;</span>
													</a>
												</td>
											</tr>
										</table>
										<!-- Button : END -->
	
									</td>
								</tr>
	
							</table>
						</td>
					</tr>
					<!-- CTA : END -->
	
					<!-- SOCIAL : BEGIN -->
					<!-- <tr>
						<td bgcolor="#292828">
							<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td style="padding: 30px 30px; text-align: center;">
										
										<table align="center" style="text-align: center;">
											<tr>
												<td>
													<i style="font-size:14px; color:#fff;"class="fab fa-twitter"></i>
												</td>
												<td width="10">&nbsp;</td>
												<td>
													<i style="font-size:14px; color:#fff;"class="fab fa-facebook"></i>
												</td>
												<td width="10">&nbsp;</td>
												<td>
													<i style="font-size:14px; color:#fff;"class="fab fa-linkedin"></i>
												</td>
											</tr>
										</table>
	
									</td>
								</tr>
	
							</table>
						</td>
					</tr> -->
					<!-- SOCIAL : END -->
	
					<!-- FOOTER : BEGIN -->
					<tr>
						<td bgcolor="#ffffff">
							<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
								
								<tr>
									<td style="padding:30px 40px 24px 40px; font-family: sans-serif; font-size: 12px; line-height: 18px; color: #666666; text-align: center; font-weight:normal;">
										<p style="margin: 0;">Copyright &copy; 2018-2019 <b>Prabandhak Inc.</b>, All Rights Reserved.</p>
									</td>
								</tr>
	
							</table>
						</td>
					</tr>
					<!-- FOOTER : END -->
	
				</table>
				<!-- Email Body : END -->
	
				<!--[if mso]>
				</td>
				</tr>
				</table>
				<![endif]-->
			</div>
	
		</center>
	</body>
	</html>
	`,
	TaskCreationMailForTeam: `<!-- THIS EMAIL WAS BUILT AND TESTED WITH LITMUS http://litmus.com -->
	<!-- IT WAS RELEASED UNDER THE MIT LICENSE https://opensource.org/licenses/MIT -->
	<!-- QUESTIONS? TWEET US @LITMUSAPP -->
	<!DOCTYPE html>
	<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
	<head>
		<meta charset="utf-8"> <!-- utf-8 works for most cases -->
		<meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
		<meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
	  <!--   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous"> -->
		<script defer src="https://use.fontawesome.com/releases/v5.0.12/js/all.js" integrity="sha384-Voup2lBiiyZYkRto2XWqbzxHXwzcm4A5RfdfG6466bu5LqjwwrjXCMBQBLMWh7qR" crossorigin="anonymous"></script>
		<title>Event - [Plain HTML]</title> <!-- The title tag shows in email notifications, like Android 4.4. -->
	
		<!-- Web Font / @font-face : BEGIN -->
		<!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->
	
		<!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
		<!--[if mso]>
			<style>
				* {
					font-family: Arial, sans-serif !important;
				}
			</style>
		<![endif]-->
	
		<!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
		<!--[if !mso]><!-->
			<!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet"> -->
		<!--<![endif]-->
	
		<!-- Web Font / @font-face : END -->
	
		<!-- CSS Reset -->
		<style>
	
			/* What it does: Remove spaces around the email design added by some email clients. */
			/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
			html,
			body {
				margin: 0 auto !important;
				padding: 0 !important;
				height: 100% !important;
				width: 100% !important;
	
			}
			
			/* What it does: Stops email clients resizing small text. */
			* {
				-ms-text-size-adjust: 100%;
				-webkit-text-size-adjust: 100%;
				font-family: monospace !important;
			}
	
			/* What it does: Centers email on Android 4.4 */
			div[style*="margin: 16px 0"] {
				margin:0 !important;
			}
	
			/* What it does: Stops Outlook from adding extra spacing to tables. */
			table,
			td {
				mso-table-lspace: 0pt !important;
				mso-table-rspace: 0pt !important;
			}
	
			/* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
			table {
				border-spacing: 0 !important;
				border-collapse: collapse !important;
				table-layout: fixed !important;
				margin: 0 auto !important;
			}
			table table table {
				table-layout: auto;
			}
	
			/* What it does: Uses a better rendering method when resizing images in IE. */
			img {
				-ms-interpolation-mode:bicubic;
			}
	
			/* What it does: A work-around for email clients meddling in triggered links. */
			*[x-apple-data-detectors],	/* iOS */
			.x-gmail-data-detectors, 	/* Gmail */
			.x-gmail-data-detectors *,
			.aBn {
				border-bottom: 0 !important;
				cursor: default !important;
				color: inherit !important;
				text-decoration: none !important;
				font-size: inherit !important;
				font-family: inherit !important;
				font-weight: inherit !important;
				line-height: inherit !important;
			}
	
			/* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */
			.a6S {
				display: none !important;
				opacity: 0.01 !important;
			}
			/* If the above doesn't work, add a .g-img class to any image in question. */
			img.g-img + div {
				display:none !important;
			   }
	
			/* What it does: Prevents underlining the button text in Windows 10 */
			.button-link {
				text-decoration: none !important;
			}
	
			/* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
			/* Create one of these media queries for each additional viewport size you'd like to fix */
			/* Thanks to Eric Lepetit @ericlepetitsf) for help troubleshooting */
			@media only screen and (min-device-width: 375px) and (max-device-width: 413px) { /* iPhone 6 and 6+ */
				.email-container {
					min-width: 375px !important;
				}
			}
			.hero_bg{
				background-color: #330033;
				background-image: url("https://reverieinc.com/prabandhak/assets/header.jpg");
			}
		</style>
	
		<!-- Progressive Enhancements -->
		<style>
	
			/* What it does: Hover styles for buttons */
			.button-td,
			.button-a {
				transition: all 100ms ease-in;
			}
			.button-td:hover,
			.button-a:hover {
				background: #555555 !important;
				border-color: #555555 !important;
			}
	
			/* Media Queries */
			@media screen and (max-width: 480px) {
	
				/* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
				.fluid {
					width: 100% !important;
					max-width: 100% !important;
					height: auto !important;
					margin-left: auto !important;
					margin-right: auto !important;
				}
	
				/* What it does: Forces table cells into full-width rows. */
				.stack-column,
				.stack-column-center {
					display: block !important;
					width: 100% !important;
					max-width: 100% !important;
					direction: ltr !important;
				}
				/* And center justify these ones. */
				.stack-column-center {
					text-align: center !important;
				}
	
				/* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
				.center-on-narrow {
					text-align: center !important;
					display: block !important;
					margin-left: auto !important;
					margin-right: auto !important;
					float: none !important;
				}
				table.center-on-narrow {
					display: inline-block !important;
				}
	
				/* What it does: Adjust typography on small screens to improve readability */
				.email-container p {
					font-size: 17px !important;
					line-height: 22px !important;
				}
			}
	
		</style>
	
		<!-- What it does: Makes background images in 72ppi Outlook render at correct size. -->
		<!--[if gte mso 9]>
		<xml>
			<o:OfficeDocumentSettings>
				<o:AllowPNG/>
				<o:PixelsPerInch>96</o:PixelsPerInch>
			</o:OfficeDocumentSettings>
		</xml>
		<![endif]-->
	
	</head>
	<body width="100%" bgcolor="#F1F1F1" style="margin: 0; mso-line-height-rule: exactly;">
		<center style="width: 100%; background: #F1F1F1; text-align: left;">
	
			<!-- Visually Hidden Preheader Text : BEGIN -->
			<div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
				Translate for India and earn while sipping coffee at home !!! 
			</div>
			<!-- Visually Hidden Preheader Text : END -->
	
			<!--
				Set the email width. Defined in two places:
				1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 680px.
				2. MSO tags for Desktop Windows Outlook enforce a 680px width.
				Note: The Fluid and Responsive templates have a different width (600px). The hybrid grid is more "fragile", and I've found that 680px is a good width. Change with caution.
			-->
			<div style="max-width: 680px; margin: auto;" class="email-container">
				<!--[if mso]>
				<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="680" align="center">
				<tr>
				<td>
				<![endif]-->
	
				<!-- Email Body : BEGIN -->
				<table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;" class="email-container">
	
	
	
	
					<!-- HERO : BEGIN -->
					<tr>
						<!-- Bulletproof Background Images c/o https://backgrounds.cm -->
						<td class="hero_bg" bgcolor="#222222" align="center" valign="top" style="text-align: center; background-position: center center !important; background-size: cover !important;">
							<!--[if gte mso 9]>
							<v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:680px; height:380px; background-position: center center !important;">
							<v:fill type="tile" src="background.png" color="#222222" />
							<v:textbox inset="0,0,0,0">
							<![endif]-->
							<div>
								<!--[if mso]>
								<table role="presentation" border="0" cellspacing="0" cellpadding="0" align="center" width="500">
								<tr>
								<td align="center" valign="middle" width="500">
								<![endif]-->
								<table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:500px; margin: auto;">
	
									<tr>
										<td height="20" style="font-size:20px; line-height:20px;">&nbsp;</td>
									</tr>
	
									<tr>
									  <td align="center" valign="middle">
										
									  <table>
										 <tr>
											 <td valign="top" style="text-align: center; padding: 60px 0 10px 0px;">
												<img src="https://reverieinc.com/prabandhak/assets/logo.png" alt="" width="60px">
												 
												 <h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 26px; line-height: 36px; color: #ffffff; font-weight: bold;">PRABANDHAK</h1>
											 </td>
										 </tr>
										 <tr>
											 <td valign="top" style="text-align: center; padding: 10px 20px 15px 20px; font-family: 'Montserrat',sans-serif; font-size: 20px; line-height: 20px; color: #fff; font-weight:300;">
												 <p>First ever AI powered translation marketplace for Indian Languages</p>
												 <p style="font-weight:400;font-size: 13px;">#IndianizeTheInternet #madeinindia</p>
											 </td>
										 </tr>
										 <tr>
											 <td valign="top" align="center" style="text-align: center; padding: 15px 0px 60px 0px;">
	
												 <!-- Button : BEGIN -->
												 <center>
												 <table role="presentation" align="center" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow" style="text-align: center;">
													 
												 </table>
												 </center>
												 <!-- Button : END -->
	
											 </td>
										 </tr> 
									  </table>
	
									  </td>
									</tr>
								
									<tr>
										<td height="20" style="font-size:20px; line-height:20px;">&nbsp;</td>
									</tr>
	
								</table>
								<!--[if mso]>
								</td>
								</tr>
								</table>
								<![endif]-->
							</div>
							<!--[if gte mso 9]>
							</v:textbox>
							</v:rect>
							<![endif]-->
						</td>
					</tr>
					<!-- HERO : END -->
	
					<!-- INTRO : BEGIN -->
					<tr>
						<td bgcolor="#ffffff">
							<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td style="padding: 40px 40px 20px 40px; text-align: left;">
										<h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 20px; line-height: 26px; color: #333333; font-weight: bold;">Greetings,</h1>
									</td>
								</tr>
								
								<tr>
									<td style="padding: 0px 40px 20px 40px; font-family: 'Montserrat', sans-serif; font-size: 15px; line-height: 26px; color: #555555; text-align: left; font-weight:300;">
										<p style="margin: 0;">!!!SENDER!!!, from !!!ORGANISATION!!! has created a new task with you on the team. Click on the "Let's Go" button below to get started.</p>
									</td>
								</tr>
								<tr>
									<td style="padding: 0px 40px 20px 40px; font-family: 'Montserrat', sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: left; font-weight:400;">
										<p style="margin: 0;">Yours sincerely,</p>
									</td>
								</tr>
	
								<tr>
									<td align="left" style="padding: 0px 40px 40px 40px;">
	
										<table width="180" align="left">
											<tr>
											  
											  <td width="110">
												
												<table width="" cellpadding="0" cellspacing="0" border="0">
												  
												  <tr>
													<td align="left" style="font-family: 'Montserrat', sans-serif; font-size:14px; line-height:20px; color:#666666;" class="body-text">
													  <p style="font-family: 'Montserrat',sans-serif; font-size:14px; line-height:20px; color:#666666; padding:0; margin:0;" class="body-text">Team Prabandhak</p>    
													</td>               
												  </tr>                            
												</table>
	
											  </td>                        
											</tr>
										</table>
	
									</td>
								</tr>
	
							</table>
						</td>
					</tr>
					<!-- INTRO : END -->
	
					<!-- AGENDA : BEGIN -->
				  <!--   <tr>
						<td bgcolor="#f7fafc">
							<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td style="padding: 40px 40px 20px 40px; text-align: left;">
										<h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 20px; line-height: 26px; color: #333333; font-weight: bold;">AGENDA</h1>
									</td>
								</tr>
								<tr>
									<td style="padding: 0px 40px 35px 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
										<p style="margin: 0;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
										quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
										consequat.</p>
									</td>
								</tr>
								<tr>
									<td style="padding: 0px 40px 20px 40px;">
										
										<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" style="border:1px solid #dddddd; border-left:3px solid #26a4d3;">
											<tr>
												<td align="" style="padding: 20px 20px 0px 20px; font-family: 'Montserrat', sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
													<h3 style="margin:0;">
														Agenda 1).
													</h3>
												</td>
											</tr>
											<tr>
												<td align="" style="padding: 20px 20px 20px 20px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
													<p style="margin:0;">
														Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
														tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
														quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
														consequat.
													</p>
												</td>
											</tr>
										</table>
	
									</td>
								</tr>
								<tr>
									<td style="padding: 0px 40px 40px 40px;">
										
										<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" style="border:1px solid #dddddd; border-left:3px solid #26a4d3;">
											<tr>
												<td align="" style="padding: 20px 20px 0px 20px; font-family: 'Montserrat', sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
													<h3 style="margin:0;">
														Agenda 2).
													</h3>
												</td>
											</tr>
											<tr>
												<td align="" style="padding: 20px 20px 20px 20px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
													<p style="margin:0;">
														Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
														tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
														quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
														consequat.
													</p>
												</td>
											</tr>
										</table>
	
									</td>
								</tr>
	
							</table>
						</td>
					</tr> -->
					<!-- AGENDA : END -->
	
					<!-- CTA : BEGIN -->
					<tr>
						<td class="hero_bg">
							<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td style="padding: 40px 40px 5px 40px; text-align: center;">
										<h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 20px; line-height: 24px; color: #ffffff; font-weight: 300;">Language is a social construct<br> It's time the society pays back</h1>
									</td>
								</tr>
								<tr>
									<td valign="middle" align="center" style="text-align: center; padding: 40px 20px 40px 20px;">
	
										<!-- Button : BEGIN -->
										<table role="presentation" align="center" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow">
											<tr>
												<td style="border-radius: 50px; background: #ffffff; text-align: center;" class="button-td">
													<a href="!!!TEMPLATE_URL!!!" style="background: #ffffff; border: 15px solid #ffffff; font-family: 'Montserrat', sans-serif; font-size: 14px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 50px; font-weight: bold;" class="button-a">
														<span style="color:#26a4d3;" class="button-link">&nbsp;&nbsp;&nbsp;&nbsp;Let's Go&nbsp;&nbsp;&nbsp;&nbsp;</span>
													</a>
												</td>
											</tr>
										</table>
										<!-- Button : END -->
	
									</td>
								</tr>
	
							</table>
						</td>
					</tr>
					<!-- CTA : END -->
	
					<!-- SOCIAL : BEGIN -->
					<!-- <tr>
						<td bgcolor="#292828">
							<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td style="padding: 30px 30px; text-align: center;">
										
										<table align="center" style="text-align: center;">
											<tr>
												<td>
													<i style="font-size:14px; color:#fff;"class="fab fa-twitter"></i>
												</td>
												<td width="10">&nbsp;</td>
												<td>
													<i style="font-size:14px; color:#fff;"class="fab fa-facebook"></i>
												</td>
												<td width="10">&nbsp;</td>
												<td>
													<i style="font-size:14px; color:#fff;"class="fab fa-linkedin"></i>
												</td>
											</tr>
										</table>
	
									</td>
								</tr>
	
							</table>
						</td>
					</tr> -->
					<!-- SOCIAL : END -->
	
					<!-- FOOTER : BEGIN -->
					<tr>
						<td bgcolor="#ffffff">
							<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
								
								<tr>
									<td style="padding:30px 40px 24px 40px; font-family: sans-serif; font-size: 12px; line-height: 18px; color: #666666; text-align: center; font-weight:normal;">
										<p style="margin: 0;">Copyright &copy; 2018-2019 <b>Prabandhak Inc.</b>, All Rights Reserved.</p>
									</td>
								</tr>
	
							</table>
						</td>
					</tr>
					<!-- FOOTER : END -->
	
				</table>
				<!-- Email Body : END -->
	
				<!--[if mso]>
				</td>
				</tr>
				</table>
				<![endif]-->
			</div>
	
		</center>
	</body>
	</html>
	`,
	TaskCreationMailForUsers: `<!-- THIS EMAIL WAS BUILT AND TESTED WITH LITMUS http://litmus.com -->
	<!-- IT WAS RELEASED UNDER THE MIT LICENSE https://opensource.org/licenses/MIT -->
	<!-- QUESTIONS? TWEET US @LITMUSAPP -->
	<!DOCTYPE html>
	<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
	<head>
		<meta charset="utf-8"> <!-- utf-8 works for most cases -->
		<meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
		<meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
	  <!--   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous"> -->
		<script defer src="https://use.fontawesome.com/releases/v5.0.12/js/all.js" integrity="sha384-Voup2lBiiyZYkRto2XWqbzxHXwzcm4A5RfdfG6466bu5LqjwwrjXCMBQBLMWh7qR" crossorigin="anonymous"></script>
		<title>Event - [Plain HTML]</title> <!-- The title tag shows in email notifications, like Android 4.4. -->
	
		<!-- Web Font / @font-face : BEGIN -->
		<!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->
	
		<!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
		<!--[if mso]>
			<style>
				* {
					font-family: Arial, sans-serif !important;
				}
			</style>
		<![endif]-->
	
		<!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
		<!--[if !mso]><!-->
			<!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet"> -->
		<!--<![endif]-->
	
		<!-- Web Font / @font-face : END -->
	
		<!-- CSS Reset -->
		<style>
	
			/* What it does: Remove spaces around the email design added by some email clients. */
			/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
			html,
			body {
				margin: 0 auto !important;
				padding: 0 !important;
				height: 100% !important;
				width: 100% !important;
	
			}
			
			/* What it does: Stops email clients resizing small text. */
			* {
				-ms-text-size-adjust: 100%;
				-webkit-text-size-adjust: 100%;
				font-family: monospace !important;
			}
	
			/* What it does: Centers email on Android 4.4 */
			div[style*="margin: 16px 0"] {
				margin:0 !important;
			}
	
			/* What it does: Stops Outlook from adding extra spacing to tables. */
			table,
			td {
				mso-table-lspace: 0pt !important;
				mso-table-rspace: 0pt !important;
			}
	
			/* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
			table {
				border-spacing: 0 !important;
				border-collapse: collapse !important;
				table-layout: fixed !important;
				margin: 0 auto !important;
			}
			table table table {
				table-layout: auto;
			}
	
			/* What it does: Uses a better rendering method when resizing images in IE. */
			img {
				-ms-interpolation-mode:bicubic;
			}
	
			/* What it does: A work-around for email clients meddling in triggered links. */
			*[x-apple-data-detectors],	/* iOS */
			.x-gmail-data-detectors, 	/* Gmail */
			.x-gmail-data-detectors *,
			.aBn {
				border-bottom: 0 !important;
				cursor: default !important;
				color: inherit !important;
				text-decoration: none !important;
				font-size: inherit !important;
				font-family: inherit !important;
				font-weight: inherit !important;
				line-height: inherit !important;
			}
	
			/* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */
			.a6S {
				display: none !important;
				opacity: 0.01 !important;
			}
			/* If the above doesn't work, add a .g-img class to any image in question. */
			img.g-img + div {
				display:none !important;
			   }
	
			/* What it does: Prevents underlining the button text in Windows 10 */
			.button-link {
				text-decoration: none !important;
			}
	
			/* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
			/* Create one of these media queries for each additional viewport size you'd like to fix */
			/* Thanks to Eric Lepetit @ericlepetitsf) for help troubleshooting */
			@media only screen and (min-device-width: 375px) and (max-device-width: 413px) { /* iPhone 6 and 6+ */
				.email-container {
					min-width: 375px !important;
				}
			}
			.hero_bg{
				background-color: #330033;
				background-image: url("https://reverieinc.com/prabandhak/assets/header.jpg");
			}
		</style>
	
		<!-- Progressive Enhancements -->
		<style>
	
			/* What it does: Hover styles for buttons */
			.button-td,
			.button-a {
				transition: all 100ms ease-in;
			}
			.button-td:hover,
			.button-a:hover {
				background: #555555 !important;
				border-color: #555555 !important;
			}
	
			/* Media Queries */
			@media screen and (max-width: 480px) {
	
				/* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
				.fluid {
					width: 100% !important;
					max-width: 100% !important;
					height: auto !important;
					margin-left: auto !important;
					margin-right: auto !important;
				}
	
				/* What it does: Forces table cells into full-width rows. */
				.stack-column,
				.stack-column-center {
					display: block !important;
					width: 100% !important;
					max-width: 100% !important;
					direction: ltr !important;
				}
				/* And center justify these ones. */
				.stack-column-center {
					text-align: center !important;
				}
	
				/* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
				.center-on-narrow {
					text-align: center !important;
					display: block !important;
					margin-left: auto !important;
					margin-right: auto !important;
					float: none !important;
				}
				table.center-on-narrow {
					display: inline-block !important;
				}
	
				/* What it does: Adjust typography on small screens to improve readability */
				.email-container p {
					font-size: 17px !important;
					line-height: 22px !important;
				}
			}
	
		</style>
	
		<!-- What it does: Makes background images in 72ppi Outlook render at correct size. -->
		<!--[if gte mso 9]>
		<xml>
			<o:OfficeDocumentSettings>
				<o:AllowPNG/>
				<o:PixelsPerInch>96</o:PixelsPerInch>
			</o:OfficeDocumentSettings>
		</xml>
		<![endif]-->
	
	</head>
	<body width="100%" bgcolor="#F1F1F1" style="margin: 0; mso-line-height-rule: exactly;">
		<center style="width: 100%; background: #F1F1F1; text-align: left;">
	
			<!-- Visually Hidden Preheader Text : BEGIN -->
			<div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
				Translate for India and earn while sipping coffee at home !!! 
			</div>
			<!-- Visually Hidden Preheader Text : END -->
	
			<!--
				Set the email width. Defined in two places:
				1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 680px.
				2. MSO tags for Desktop Windows Outlook enforce a 680px width.
				Note: The Fluid and Responsive templates have a different width (600px). The hybrid grid is more "fragile", and I've found that 680px is a good width. Change with caution.
			-->
			<div style="max-width: 680px; margin: auto;" class="email-container">
				<!--[if mso]>
				<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="680" align="center">
				<tr>
				<td>
				<![endif]-->
	
				<!-- Email Body : BEGIN -->
				<table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;" class="email-container">
	
	
	
	
					<!-- HERO : BEGIN -->
					<tr>
						<!-- Bulletproof Background Images c/o https://backgrounds.cm -->
						<td class="hero_bg" bgcolor="#222222" align="center" valign="top" style="text-align: center; background-position: center center !important; background-size: cover !important;">
							<!--[if gte mso 9]>
							<v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:680px; height:380px; background-position: center center !important;">
							<v:fill type="tile" src="background.png" color="#222222" />
							<v:textbox inset="0,0,0,0">
							<![endif]-->
							<div>
								<!--[if mso]>
								<table role="presentation" border="0" cellspacing="0" cellpadding="0" align="center" width="500">
								<tr>
								<td align="center" valign="middle" width="500">
								<![endif]-->
								<table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:500px; margin: auto;">
	
									<tr>
										<td height="20" style="font-size:20px; line-height:20px;">&nbsp;</td>
									</tr>
	
									<tr>
									  <td align="center" valign="middle">
										
									  <table>
										 <tr>
											 <td valign="top" style="text-align: center; padding: 60px 0 10px 0px;">
												<img src="https://reverieinc.com/prabandhak/assets/logo.png" alt="" width="60px">
												 
												 <h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 26px; line-height: 36px; color: #ffffff; font-weight: bold;">PRABANDHAK</h1>
											 </td>
										 </tr>
										 <tr>
											 <td valign="top" style="text-align: center; padding: 10px 20px 15px 20px; font-family: 'Montserrat',sans-serif; font-size: 20px; line-height: 20px; color: #fff; font-weight:300;">
												 <p>First ever AI powered translation marketplace for Indian Languages</p>
												 <p style="font-weight:400;font-size: 13px;">#IndianizeTheInternet #madeinindia</p>
											 </td>
										 </tr>
										 <tr>
											 <td valign="top" align="center" style="text-align: center; padding: 15px 0px 60px 0px;">
	
												 <!-- Button : BEGIN -->
												 <center>
												 <table role="presentation" align="center" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow" style="text-align: center;">
													 
												 </table>
												 </center>
												 <!-- Button : END -->
	
											 </td>
										 </tr> 
									  </table>
	
									  </td>
									</tr>
								
									<tr>
										<td height="20" style="font-size:20px; line-height:20px;">&nbsp;</td>
									</tr>
	
								</table>
								<!--[if mso]>
								</td>
								</tr>
								</table>
								<![endif]-->
							</div>
							<!--[if gte mso 9]>
							</v:textbox>
							</v:rect>
							<![endif]-->
						</td>
					</tr>
					<!-- HERO : END -->
	
					<!-- INTRO : BEGIN -->
					<tr>
						<td bgcolor="#ffffff">
							<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td style="padding: 40px 40px 20px 40px; text-align: left;">
										<h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 20px; line-height: 26px; color: #333333; font-weight: bold;">Greetings,</h1>
									</td>
								</tr>
								<tr>
									<td style="padding: 0px 40px 20px 40px; font-family: 'Montserrat', sans-serif; font-size: 15px; line-height: 26px; color: #555555; text-align: left; font-weight:300;">
										<p style="margin: 0;">A new task has been added to the prabandhak portal. Click on the "Let's Go" button below to start applying for the task.</p>
									</td>
								</tr>
								<tr>
									<td style="padding: 0px 40px 20px 40px; font-family: 'Montserrat', sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: left; font-weight:400;">
										<p style="margin: 0;">Yours sincerely,</p>
									</td>
								</tr>
	
								<tr>
									<td align="left" style="padding: 0px 40px 40px 40px;">
	
										<table width="180" align="left">
											<tr>
											  
											  <td width="110">
												
												<table width="" cellpadding="0" cellspacing="0" border="0">
												  
												  <tr>
													<td align="left" style="font-family: 'Montserrat', sans-serif; font-size:14px; line-height:20px; color:#666666;" class="body-text">
													  <p style="font-family: 'Montserrat',sans-serif; font-size:14px; line-height:20px; color:#666666; padding:0; margin:0;" class="body-text">Team Prabandhak</p>    
													</td>               
												  </tr>                            
												</table>
	
											  </td>                        
											</tr>
										</table>
	
									</td>
								</tr>
	
							</table>
						</td>
					</tr>
					<!-- INTRO : END -->
	
					<!-- AGENDA : BEGIN -->
				  <!--   <tr>
						<td bgcolor="#f7fafc">
							<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td style="padding: 40px 40px 20px 40px; text-align: left;">
										<h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 20px; line-height: 26px; color: #333333; font-weight: bold;">AGENDA</h1>
									</td>
								</tr>
								<tr>
									<td style="padding: 0px 40px 35px 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
										<p style="margin: 0;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
										quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
										consequat.</p>
									</td>
								</tr>
								<tr>
									<td style="padding: 0px 40px 20px 40px;">
										
										<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" style="border:1px solid #dddddd; border-left:3px solid #26a4d3;">
											<tr>
												<td align="" style="padding: 20px 20px 0px 20px; font-family: 'Montserrat', sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
													<h3 style="margin:0;">
														Agenda 1).
													</h3>
												</td>
											</tr>
											<tr>
												<td align="" style="padding: 20px 20px 20px 20px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
													<p style="margin:0;">
														Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
														tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
														quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
														consequat.
													</p>
												</td>
											</tr>
										</table>
	
									</td>
								</tr>
								<tr>
									<td style="padding: 0px 40px 40px 40px;">
										
										<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" style="border:1px solid #dddddd; border-left:3px solid #26a4d3;">
											<tr>
												<td align="" style="padding: 20px 20px 0px 20px; font-family: 'Montserrat', sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
													<h3 style="margin:0;">
														Agenda 2).
													</h3>
												</td>
											</tr>
											<tr>
												<td align="" style="padding: 20px 20px 20px 20px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
													<p style="margin:0;">
														Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
														tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
														quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
														consequat.
													</p>
												</td>
											</tr>
										</table>
	
									</td>
								</tr>
	
							</table>
						</td>
					</tr> -->
					<!-- AGENDA : END -->
	
					<!-- CTA : BEGIN -->
					<tr>
						<td class="hero_bg">
							<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td style="padding: 40px 40px 5px 40px; text-align: center;">
										<h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 20px; line-height: 24px; color: #ffffff; font-weight: 300;">Language is a social construct<br> It's time the society pays back</h1>
									</td>
								</tr>
								<tr>
									<td valign="middle" align="center" style="text-align: center; padding: 40px 20px 40px 20px;">
	
										<!-- Button : BEGIN -->
										<table role="presentation" align="center" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow">
											<tr>
												<td style="border-radius: 50px; background: #ffffff; text-align: center;" class="button-td">
													<a href="!!!TEMPLATE_URL!!!" style="background: #ffffff; border: 15px solid #ffffff; font-family: 'Montserrat', sans-serif; font-size: 14px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 50px; font-weight: bold;" class="button-a">
														<span style="color:#26a4d3;" class="button-link">&nbsp;&nbsp;&nbsp;&nbsp;Let's Go&nbsp;&nbsp;&nbsp;&nbsp;</span>
													</a>
												</td>
											</tr>
										</table>
										<!-- Button : END -->
	
									</td>
								</tr>
	
							</table>
						</td>
					</tr>
					<!-- CTA : END -->
	
					<!-- SOCIAL : BEGIN -->
					<!-- <tr>
						<td bgcolor="#292828">
							<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td style="padding: 30px 30px; text-align: center;">
										
										<table align="center" style="text-align: center;">
											<tr>
												<td>
													<i style="font-size:14px; color:#fff;"class="fab fa-twitter"></i>
												</td>
												<td width="10">&nbsp;</td>
												<td>
													<i style="font-size:14px; color:#fff;"class="fab fa-facebook"></i>
												</td>
												<td width="10">&nbsp;</td>
												<td>
													<i style="font-size:14px; color:#fff;"class="fab fa-linkedin"></i>
												</td>
											</tr>
										</table>
	
									</td>
								</tr>
	
							</table>
						</td>
					</tr> -->
					<!-- SOCIAL : END -->
	
					<!-- FOOTER : BEGIN -->
					<tr>
						<td bgcolor="#ffffff">
							<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
								
								<tr>
									<td style="padding:30px 40px 24px 40px; font-family: sans-serif; font-size: 12px; line-height: 18px; color: #666666; text-align: center; font-weight:normal;">
										<p style="margin: 0;">Copyright &copy; 2018-2019 <b>Prabandhak Inc.</b>, All Rights Reserved.</p>
									</td>
								</tr>
	
							</table>
						</td>
					</tr>
					<!-- FOOTER : END -->
	
				</table>
				<!-- Email Body : END -->
	
				<!--[if mso]>
				</td>
				</tr>
				</table>
				<![endif]-->
			</div>
	
		</center>
	</body>
	</html>
	`,
	alphaInvite:`

    <!DOCTYPE html>
    <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <meta charset="utf-8"> <!-- utf-8 works for most cases -->
        <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
        <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
      <!--   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous"> -->
        <script defer src="https://use.fontawesome.com/releases/v5.0.12/js/all.js" integrity="sha384-Voup2lBiiyZYkRto2XWqbzxHXwzcm4A5RfdfG6466bu5LqjwwrjXCMBQBLMWh7qR" crossorigin="anonymous"></script>
        <title>Prabandhak</title> <!-- The title tag shows in email notifications, like Android 4.4. -->
    
        <!-- Web Font / @font-face : BEGIN -->
        <!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->
    
        <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
        <!--[if mso]>
            <style>
                * {
                    font-family: Arial, sans-serif !important;
                }
            </style>
        <![endif]-->
    
        <!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
        <!--[if !mso]><!-->
            <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet"> -->
        <!--<![endif]-->
    
        <!-- Web Font / @font-face : END -->
    
        <!-- CSS Reset -->
        <style>
    
            /* What it does: Remove spaces around the email design added by some email clients. */
            /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
            html,
            body {
                margin: 0 auto !important;
                padding: 0 !important;
                height: 100% !important;
                width: 100% !important;
    
            }
            
            /* What it does: Stops email clients resizing small text. */
            * {
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
                font-family: monospace !important;
            }
    
            /* What it does: Centers email on Android 4.4 */
            div[style*="margin: 16px 0"] {
                margin:0 !important;
            }
    
            /* What it does: Stops Outlook from adding extra spacing to tables. */
            table,
            td {
                mso-table-lspace: 0pt !important;
                mso-table-rspace: 0pt !important;
            }
    
            /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
            table {
                border-spacing: 0 !important;
                border-collapse: collapse !important;
                table-layout: fixed !important;
                margin: 0 auto !important;
            }
            table table table {
                table-layout: auto;
            }
    
            /* What it does: Uses a better rendering method when resizing images in IE. */
            img {
                -ms-interpolation-mode:bicubic;
            }
    
            /* What it does: A work-around for email clients meddling in triggered links. */
            *[x-apple-data-detectors],  /* iOS */
            .x-gmail-data-detectors,    /* Gmail */
            .x-gmail-data-detectors *,
            .aBn {
                border-bottom: 0 !important;
                cursor: default !important;
                color: inherit !important;
                text-decoration: none !important;
                font-size: inherit !important;
                font-family: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
            }
    
            /* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */
            .a6S {
                display: none !important;
                opacity: 0.01 !important;
            }
            /* If the above doesn't work, add a .g-img class to any image in question. */
            img.g-img + div {
                display:none !important;
               }
    
            /* What it does: Prevents underlining the button text in Windows 10 */
            .button-link {
                text-decoration: none !important;
            }
    
            /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
            /* Create one of these media queries for each additional viewport size you'd like to fix */
            /* Thanks to Eric Lepetit @ericlepetitsf) for help troubleshooting */
            @media only screen and (min-device-width: 375px) and (max-device-width: 413px) { /* iPhone 6 and 6+ */
                .email-container {
                    min-width: 375px !important;
                }
            }
            .hero_bg{
                background-color: #330033;
                background-image: url("https://reverieinc.com/prabandhak/assets/header.jpg");
            }
        </style>
    
        <!-- Progressive Enhancements -->
        <style>
    
            /* What it does: Hover styles for buttons */
            .button-td,
            .button-a {
                transition: all 100ms ease-in;
            }
            .button-td:hover,
            .button-a:hover {
                background: #555555 !important;
                border-color: #555555 !important;
            }
    
            /* Media Queries */
            @media screen and (max-width: 480px) {
    
                /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
                .fluid {
                    width: 100% !important;
                    max-width: 100% !important;
                    height: auto !important;
                    margin-left: auto !important;
                    margin-right: auto !important;
                }
    
                /* What it does: Forces table cells into full-width rows. */
                .stack-column,
                .stack-column-center {
                    display: block !important;
                    width: 100% !important;
                    max-width: 100% !important;
                    direction: ltr !important;
                }
                /* And center justify these ones. */
                .stack-column-center {
                    text-align: center !important;
                }
    
                /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
                .center-on-narrow {
                    text-align: center !important;
                    display: block !important;
                    margin-left: auto !important;
                    margin-right: auto !important;
                    float: none !important;
                }
                table.center-on-narrow {
                    display: inline-block !important;
                }
    
                /* What it does: Adjust typography on small screens to improve readability */
                .email-container p {
                    font-size: 17px !important;
                    line-height: 22px !important;
                }
            }
    
        </style>
    
        <!-- What it does: Makes background images in 72ppi Outlook render at correct size. -->
        <!--[if gte mso 9]>
        <xml>
            <o:OfficeDocumentSettings>
                <o:AllowPNG/>
                <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
    
    </head>
    <body width="100%" bgcolor="#F1F1F1" style="margin: 0; mso-line-height-rule: exactly;">
        <center style="width: 100%; background: #F1F1F1; text-align: left;">
    
            <!-- Visually Hidden Preheader Text : BEGIN -->
            <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
                Translate for India and earn while sipping coffee at home !!! 
            </div>
            <!-- Visually Hidden Preheader Text : END -->
    
            <!--
                Set the email width. Defined in two places:
                1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 680px.
                2. MSO tags for Desktop Windows Outlook enforce a 680px width.
                Note: The Fluid and Responsive templates have a different width (600px). The hybrid grid is more "fragile", and I've found that 680px is a good width. Change with caution.
            -->
            <div style="max-width: 680px; margin: auto;" class="email-container">
                <!--[if mso]>
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="680" align="center">
                <tr>
                <td>
                <![endif]-->
    
                <!-- Email Body : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;" class="email-container">
    
    
    
    
                    <!-- HERO : BEGIN -->
                    <tr>
                        <!-- Bulletproof Background Images c/o https://backgrounds.cm -->
                        <td class="hero_bg" bgcolor="#222222" align="center" valign="top" style="text-align: center; background-position: center center !important; background-size: cover !important;">
                            <!--[if gte mso 9]>
                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:680px; height:380px; background-position: center center !important;">
                            <v:fill type="tile" src="background.png" color="#222222" />
                            <v:textbox inset="0,0,0,0">
                            <![endif]-->
                            <div>
                                <!--[if mso]>
                                <table role="presentation" border="0" cellspacing="0" cellpadding="0" align="center" width="500">
                                <tr>
                                <td align="center" valign="middle" width="500">
                                <![endif]-->
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:500px; margin: auto;">
    
                                    <tr>
                                        <td height="20" style="font-size:20px; line-height:20px;">&nbsp;</td>
                                    </tr>
    
                                    <tr>
                                      <td align="center" valign="middle">
                                        
                                      <table>
                                         <tr>
                                             <td valign="top" style="text-align: center; padding: 60px 0 10px 0px;">
                                                <img src="https://reverieinc.com/prabandhak/assets/logo.png" alt="" width="60px">
                                                 
                                                 <h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 26px; line-height: 36px; color: #ffffff; font-weight: bold;">PRABANDHAK</h1>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td valign="top" style="text-align: center; padding: 10px 20px 15px 20px; font-family: 'Montserrat',sans-serif; font-size: 20px; line-height: 20px; color: #fff; font-weight:300;">
                                                 <p>First ever Machine assisted translation marketplace for Indian Languages</p>
                                                 <p style="font-weight:400;font-size: 13px;">#IndianizeTheInternet #madeinindia</p>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td valign="top" align="center" style="text-align: center; padding: 15px 0px 60px 0px;">
    
                                                 <!-- Button : BEGIN -->
                                                 <center>
                                                 <table role="presentation" align="center" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow" style="text-align: center;">
                                                     
                                                 </table>
                                                 </center>
                                                 <!-- Button : END -->
    
                                             </td>
                                         </tr> 
                                      </table>
    
                                      </td>
                                    </tr>
                                
                                    <tr>
                                        <td height="20" style="font-size:20px; line-height:20px;">&nbsp;</td>
                                    </tr>
    
                                </table>
                                <!--[if mso]>
                                </td>
                                </tr>
                                </table>
                                <![endif]-->
                            </div>
                            <!--[if gte mso 9]>
                            </v:textbox>
                            </v:rect>
                            <![endif]-->
                        </td>
                    </tr>
                    <!-- HERO : END -->
    
                    <!-- INTRO : BEGIN -->
                    <tr>
                        <td bgcolor="#ffffff">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td style="padding: 40px 40px 20px 40px; text-align: left;">
                                        <h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 16px; line-height: 26px; color: #333333; font-weight: bold;">Greetings,</h1>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0px 40px 20px 40px; font-family: 'Montserrat', sans-serif; font-size: 15px; line-height: 26px; color: #555555; text-align: left; font-weight:300;">
                                        <p style="margin: 0;">We are delighted to invite you for a demo of <strong>Prabandhak</strong>,India's first machine assisted translation toolkit with an integrated marketplace.</p><br>
                                        <p style="margin: 0;">Our intelligent CAT tool provides features like -
                                            <ul>
                                                <li>Automatic Translation/Transliteration Memory</li>
                                                <li>Automatic Fragment Memory generator</li>
                                                <li>Indic Spellchecker™</li>
                                                <li>Translation Memory - Machine Translation convertor</li>
                                                <li>Automatic QA/QC</li>
                                                <li>Instructions Terminology</li>
                                                <li>Swalekh™ Input for Indian lanugage typing</li>
                                                <li>English to 11 Indian language dictionary/synonyms</li>
                                            </ul>
                                            Prabandhak's inbuilt marketplace helps you discover new translation jobs through the platform and helps you take advantage of the entire translation community.
                                        </p><br>
                                        <p style="margin: 0;">We are currently in our alpha-testing mode where we are offering free credits to all our Alpha customers.</p><br>
                                        <p style="margin: 0;">Hope you find <strong>Prabandhak</strong> useful for your business.</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0px 40px 20px 40px; font-family: 'Montserrat', sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: left; font-weight:400;">
                                        <p style="margin: 0;">Yours sincerely,</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0px 40px 20px 40px; font-family: 'Montserrat', sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: left; font-weight:600;">
                                        <p style="margin: 0;">Team Prabandhak</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- INTRO : END -->
    
                    <!-- AGENDA : BEGIN -->
                  <!--   <tr>
                        <td bgcolor="#f7fafc">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td style="padding: 40px 40px 20px 40px; text-align: left;">
                                        <h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 20px; line-height: 26px; color: #333333; font-weight: bold;">AGENDA</h1>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0px 40px 35px 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
                                        <p style="margin: 0;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat.</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0px 40px 20px 40px;">
                                        
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" style="border:1px solid #dddddd; border-left:3px solid #26a4d3;">
                                            <tr>
                                                <td align="" style="padding: 20px 20px 0px 20px; font-family: 'Montserrat', sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
                                                    <h3 style="margin:0;">
                                                        Agenda 1).
                                                    </h3>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="" style="padding: 20px 20px 20px 20px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
                                                    <p style="margin:0;">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                        consequat.
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>
    
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0px 40px 40px 40px;">
                                        
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" style="border:1px solid #dddddd; border-left:3px solid #26a4d3;">
                                            <tr>
                                                <td align="" style="padding: 20px 20px 0px 20px; font-family: 'Montserrat', sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
                                                    <h3 style="margin:0;">
                                                        Agenda 2).
                                                    </h3>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="" style="padding: 20px 20px 20px 20px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
                                                    <p style="margin:0;">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                        consequat.
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>
    
                                    </td>
                                </tr>
    
                            </table>
                        </td>
                    </tr> -->
                    <!-- AGENDA : END -->
    
                    <!-- CTA : BEGIN -->
                    <tr>
                        <td class="hero_bg">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td style="padding: 40px 40px 5px 40px; text-align: center;">
                                        <h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 20px; line-height: 24px; color: #ffffff; font-weight: 300;">Language is a social construct<br> It's time the society pays back</h1>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle" align="center" style="text-align: center; padding: 40px 20px 40px 20px;">
    
                                        <!-- Button : BEGIN --
                                        <table role="presentation" align="center" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow">
                                            <tr>
                                                <td style="border-radius: 50px; background: #ffffff; text-align: center;" class="button-td">
                                                    <a href="!!!TEMPLATE_URL!!!" style="background: #ffffff; border: 15px solid #ffffff; font-family: 'Montserrat', sans-serif; font-size: 14px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 50px; font-weight: bold;" class="button-a">
                                                        <span style="color:#26a4d3;" class="button-link">&nbsp;&nbsp;&nbsp;&nbsp;Let's Go&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                        !-- Button : END -->
    
                                    </td>
                                </tr>
    
                            </table>
                        </td>
                    </tr>
                    <!-- CTA : END -->
    
                    <!-- SOCIAL : BEGIN -->
                    <!-- <tr>
                        <td bgcolor="#292828">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td style="padding: 30px 30px; text-align: center;">
                                        
                                        <table align="center" style="text-align: center;">
                                            <tr>
                                                <td>
                                                    <i style="font-size:14px; color:#fff;"class="fab fa-twitter"></i>
                                                </td>
                                                <td width="10">&nbsp;</td>
                                                <td>
                                                    <i style="font-size:14px; color:#fff;"class="fab fa-facebook"></i>
                                                </td>
                                                <td width="10">&nbsp;</td>
                                                <td>
                                                    <i style="font-size:14px; color:#fff;"class="fab fa-linkedin"></i>
                                                </td>
                                            </tr>
                                        </table>
    
                                    </td>
                                </tr>
    
                            </table>
                        </td>
                    </tr> -->
                    <!-- SOCIAL : END -->
    
                    <!-- FOOTER : BEGIN -->
                    <tr>
                        <td bgcolor="#ffffff">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                
                                <tr>
                                    <td style="padding:30px 40px 24px 40px; font-family: sans-serif; font-size: 12px; line-height: 18px; color: #666666; text-align: center; font-weight:normal;">
                                        <p style="margin: 0;">Copyright &copy; 2018-2019 <b>Prabandhak Inc.</b>, All Rights Reserved.</p>
                                    </td>
                                </tr>
    
                            </table>
                        </td>
                    </tr>
                    <!-- FOOTER : END -->
    
                </table>
                <!-- Email Body : END -->
    
                <!--[if mso]>
                </td>
                </tr>
                </table>
                <![endif]-->
            </div>
    
        </center>
    </body>
    </html>
	`
}

module.exports={makeTemplate,invite}
