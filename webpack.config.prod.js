import webpack from 'webpack';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import WebpackMd5Hash from 'webpack-md5-hash';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import CleanWebpackPlugin from 'clean-webpack-plugin';
import path from 'path';

process.noDeprecation = true

const GLOBALS = {
  'process.env.NODE_ENV': JSON.stringify('production'),
  // __DEV__: false
}

export default {
    resolve: {
    enforceExtension: false
    // "alias": {
    //   "react": "preact-compat",
    //   "react-dom": "preact-compat"
    // }
  },

  devtool: 'inline-source-map',

  context: path.resolve(__dirname, './app'),

  entry: {
    bundle: path.resolve(__dirname, 'app/src/main')
  },

  output: {
    path: path.resolve(__dirname, 'node/public'),
    publicPath: '/',
    filename: 'src/[name].[chunkhash].js'
  },

  plugins: [

    // defines GLOBALS, in our case we are setting the production flag
    new webpack.DefinePlugin(GLOBALS),

    new WebpackMd5Hash(),

    new CleanWebpackPlugin(['node/public'], {
      root: __dirname,
      verbose: true,
      dry: false
    }),

    // new webpack.optimize.CommonsChunkPlugin({
    //   name: 'commons',
    //   filename: 'scripts/commons.js',
    //   minChunks: 2,
    // }),

    new ExtractTextPlugin("styles/[name].[hash].css"),

    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false
    }),

    //Minify JS
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.optimize.AggressiveMergingPlugin(),//Merge chunks
  
    new HtmlWebpackPlugin({
      template: '../tools/template.html',
      inject: 'body',
      cache: false
    }),

    // copy static resources
    new CopyWebpackPlugin([{
      from: 'images',
      to: 'images'
    },
    {
      from: 'sounds',
      to: 'sounds'
    }], {
      copyUnmodified: true
    }),
    new webpack.ProvidePlugin({
      jQuery: 'jquery',
      $: 'jquery'
    })
  ],

  /**
   * Module loaders
   */
    module: {
      rules: [{
          test: /\.js$/,
          include: [ path.resolve(__dirname, 'app/src') ],
          use: [{ 
            loader: 'babel-loader',
            options: { cacheDirectory: './.cache' }
          }]
        },
        { test: /\.json$/, use: ["json-loader"] },

        { test: /\.css$/, use: ['style-loader', 'css-loader'] },
        { test: /\.(sass|scss)$/, use: ["style-loader", "css-loader", "postcss-loader", "sass-loader"] },
        
        {test: /\.(jpe?g)?$/, use: [{ loader: 'url-loader', options:{ mimetype: 'image/jpeg'} } ] },
        {test: /\.(png)$/, use: [{ loader: 'url-loader', options: { mimetype: 'image/png' } } ] },
        {test: /\.(gif)$/, use: [{ loader: 'url-loader', options: { mimetype: 'image/gif' } } ] },
        {test: /\.(svg)$/, use: [{ loader: 'url-loader', options: { mimetype: 'image/svg' } } ] },
        
        { test: /\.ttf(.*)$/, use: [{ loader: 'url-loader', options: { name:"fonts/[name].[ext]", limit:5000 } }]},
        { test: /\.eot(.*)$/, use: [{ loader: 'url-loader', options: { name:"fonts/[name].[ext]", limit:5000 } }]},
        { test: /\.(woff|woff2)(.*)$/, use: [{ loader: 'url-loader', options: { name:"fonts/[name].[ext]", limit:5000 } }]}
      ]
    }
};



//
