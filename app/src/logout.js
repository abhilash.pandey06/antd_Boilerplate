// auth.js
import Keycloak from 'keycloak-js'


var KeycloakObj = Keycloak({
  //...kcConfig,

  "clientId": "rev_data_manager",
  "url": "http://beta.auth.revup.reverieinc.com/auth",
  "realm": "apiman"
})

export default function(mountCallback) {
  
  let parts = location.href.split('?'),
      qstr = ''
  if(parts.length > 1)
    qstr = parts[1]

  if(/client-upload=1/.test(qstr)) {
    mountCallback({ client: true, tokenParsed: { email: 'client' } })
    return
  }

  KeycloakObj.init({ onLoad: 'check-sso', flow: 'hybrid', checkLoginIframe: false }).success(() => {
    // already logged in
    if(KeycloakObj.token) {
      mountCallback(KeycloakObj)
    }
    // not authenticated
    else {
      KeycloakObj.login()
    }
  }).error(function() {
    alert("Not authenticate")
  })

  return KeycloakObj
}

export function getKcObj() {
  return KeycloakObj
}

export function logOut() {
  // KeycloakObj.logout()
 let currentUser = JSON.parse(window.localStorage.getItem("prabandhak_user"))
 currentUser.loginTime=0;
 window.localStorage.setItem("prabandhak_user",JSON.stringify(currentUser))
 window.location.reload()

}
