import React from 'react'
//import antd utility
import {notification,Icon} from 'antd';

//on Warning 
export function openNotificationWithIcon(type,title){
  notification[type]({
    message: title
  });
};

//on Successfull Event
export function sucessFully(msg) {
  notification.open({
    message: msg,
    icon: <Icon type="smile" style={{ color: '#108ee9' }} />
  });
};