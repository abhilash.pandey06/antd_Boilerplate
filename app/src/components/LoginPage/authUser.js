import React, { Component } from 'react'
import Root from './rootContainer'
import AuthUI from './authContainer'
import { Route, Redirect, Switch } from 'react-router'


import moment from 'moment'
// import HomePage from '../components/homepage'

export default class AuthUser extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        authorised: false,memoryUser: null,busy: true,homepage: true
      }
      this.checkMemory = this.checkMemory.bind(this)
      this.validateUser = this.validateUser.bind(this)
    }
  
    componentDidMount() {
      this.checkMemory()
    }
  
    checkMemory() {
      let curTime = moment().unix()
      let memory = JSON.parse(window.localStorage.getItem("prabandhak_user")?window.localStorage.getItem("prabandhak_user"):null)
      let expiry;
      if(memory) {
        expiry = moment(memory.loginTime *1000).add(20,'minutes').unix()
      }
      if(memory===null) {
        this.setState({authorised: false, busy: false})
      } else if(memory!==null && expiry<curTime) {
        this.setState({authorised: false, memoryUser: memory, busy: false})
      } else {
        this.setState({authorised: true, memoryUser: memory, busy: false})
        this.validateUser({tokenParsed:{"email":memory.user}})
      }
    }

    validateUser(userObj) {
        let memory = {user: userObj.tokenParsed.email, loginTime: moment().unix()}
        this.setState({authorised: true, memoryUser: memory})
        window.localStorage.setItem("prabandhak_user",JSON.stringify(memory))
    }
  
    render() {
      return (
            <div>
                {!this.state.busy && 
                    <Route path='*' render={props => {  
                        if(!this.state.memoryUser) {
                            return <AuthUI {...props} validateUser={this.validateUser} />
                        } else if(!this.state.authorised && this.state.memoryUser && this.state.memoryUser.user) {
                            return <AuthUI {...props} memoryUser={this.state.memoryUser.user} validateUser={this.validateUser} />
                        } else if(this.state.authorised && this.state.memoryUser && this.state.memoryUser.user) {
                            return <Root {...props} kcUser={this.state.memoryUser.user} />
                        }
                    }} />
                }
            </div>
        )
    }
}