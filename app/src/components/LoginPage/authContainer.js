import React from 'react';
// import '../../../styles/animations.css';
import axios from 'axios'
// import '../../../styles/_auth.scss'
import config from '../../../../backend/config';
//toater alert
import { openNotificationWithIcon, sucessFully } from '../Notification/Notify';

import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';

import { Form, Icon, Input, Button, Checkbox, notification } from 'antd';

const FormItem = Form.Item;

/**
 * The root container
 */
export default class AuthUI extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            memoryUser: this.props.memoryUser, confirmPassword: null, oldPassword: null,
            newPassword: null, infoType: null, formType: "login", email: "", password: "",
            isChecked: false
        }
        this.changeFormType = this.changeFormType.bind(this)
        this.handleFormChange = this.handleFormChange.bind(this)
        this.formAction = this.formAction.bind(this)
        this.facebookLogin = this.facebookLogin.bind(this)
        // this.clearMemory=this.clearMemory.bind(this)
    }
//handle form change action functions
    handleFormChange(type, e) {
        let curState = this.state
        curState[type] = e.target.value
        this.setState(curState)
    }


//component did for validating user auth
    componentDidMount() {
        const authCode = ''
        if (authCode && authCode !== "") {
            axios.post("/auth/validate/auth-code", { authCode })
                .then(res => {
                    if (res.data.success) {
                        setTimeout(() => {
                            this.setState({ formType: "login", busy: false })
                        }, 3000)
                    }
                })
        }
    }
    //handle form change action functions
    changeFormType(e) {
        console.log(e);
        this.setState({ formType: e.target.name })
    }
    //form changed based on action perfom 
    formAction(e) {
        e.preventDefault()
        let formType = this.state.formType
        //check formType 
        if (formType === "signup") {
            //verifying Details
            if (this.state.email !== "" && this.state.password !== "" && this.state.confirmPassword !== "") {           //verifying password
                if (this.state.password === this.state.confirmPassword) {
                    let data = {
                        username: this.state.email,
                        password: this.state.password,
                    }
                    //post method for registerd new Users
                    axios.post(config.baseUrl + "/auth/register/user", data)
                        .then(res => {
                            //if error
                            if (res.data.errorMessage) {
                                return openNotificationWithIcon('error', res.data.errorMessage)
                            }//on Successfull Registered
                            else {
                                this.setState({ formType: "login" })
                                return sucessFully("Successfully registered user. Please verify your e-mail.");
                            }
                        })//catch error message
                        .catch(err => {
                            this.setState({ formType: "signup" })
                            return openNotificationWithIcon('error', err.message)
                        })
                }//pass wwrong
                else {
                    return openNotificationWithIcon('warning', "Your passwords don't seem to be matching.")
                }
            }//Warning notification
            else {
                return openNotificationWithIcon('warning', "One or more fields seem to be empty. All fields are mandatory.")
            }
        }
        //Default login Page
        if (formType === "login" || formType === "lockscreen") {
            if (this.state.email === "") {
                //warning notification
                return openNotificationWithIcon('warning', 'Please enter registered email Id ')
            } else if (this.state.password === "") {
                //warning notifiaction
                return openNotificationWithIcon('warning', 'Please enter password')
            }
            if (this.state.email !== "" && this.state.password !== "") {
                //info
                let info = { username: this.state.email, password: this.state.password }
                //post method to login User
                axios.post(config.baseUrl + "/auth/login/user", info)
                    .then(res => {
                        //error
                        if (res.data.error) {
                            return openNotificationWithIcon('error', res.data.error_description)
                        }//if okay
                        else {
                            this.props.validateUser({ tokenParsed: { "email": this.state.email } })
                            return sucessFully('login successfully')
                        }
                    })
            }

        }
        if (formType === "forgetForm") {
            //verifying email id
            if (this.state.email !== "") {
                //post request for forgert password event
                axios.post(config.baseUrl + "/auth/forgot/password", { "username": this.state.email })
                    .then(res => {
                        //if success
                        if (res.data.success) {
                            this.setState({ formType: "login" })
                            return openNotificationWithIcon("info", "Please check in registered mail inbox.")
                        }
                    })
            }
            else {
                return openNotificationWithIcon('warning', "Cannot email to an empty e-mail address..!!")
            }
        }
        if (formType === "reset-password") {
            //verifying details user info............
            if (this.state.email !== "" && this.state.oldPassword !== "" && this.state.newPassword !== "") {
                //post request to reset password .........
                axios.post(config.baseUrl + "/auth/reset/password", {
                    username: this.state.email,
                    password: this.state.oldPassword,
                    newPassword: this.state.newPassword
                })
                    .then(res => {
                        //if success
                        if (res.data.success) {
                            this.setState({ formType: "login" })
                            return sucessFully("Password reset Successfully.")
                        }
                        else {
                            this.setState({ formType: "reset-password" })
                            return openNotificationWithIcon('error', 'Enter the valid Current Password')
                        }
                    })
            } else {
                return openNotificationWithIcon('error', 'Enter all the details')
            }
        }
    }
    //facebook login handle login 
    facebookLogin = (res) => {
        this.setState({ email: res.email })
        if (this.state.email) {
            this.props.validateUser({ tokenParsed: { email: this.state.email } })
            sucessFully('login successfully')
        }
    }
//clear memory
    // clearMemory(){
    //     this.setState({email:"",formType:"lockscreen"})
    //     window.localStorage.setItem("prabandhak_user","")
    // }

    render() {
        const responseFacebook = (response) => {
            console.log(response);
            this.facebookLogin(response);
        }
        const componentClicked = () => {
            console.log('clicked')
        }

        const responseGoogle = (response) => {
            console.log(response);
        }

        if (this.state.formType == "login") {
            return (<section className="bg-img">
                <Form onSubmit={this.handleSubmit} className="login-form">
                    <FormItem style={{ color: "white", fontSize: "25px", textAlign: "center" }}> Login</FormItem>
                    <FormItem>
                        <Input
                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            value={this.state.email}
                            placeholder="Email"
                            onChange={this.handleFormChange.bind(0, 'email')} />
                    </FormItem>
                    <FormItem>
                        <Input
                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            type="password"
                            placeholder="Password"
                            value={this.state.password}
                            onChange={this.handleFormChange.bind(0, 'password')} />
                    </FormItem>
                    <FormItem>
                        <Checkbox style={{
                            backgroundColor: "transparent",
                            color: "white",
                            borderColor: "transparent",

                        }} >
                            Remember me
                        </Checkbox>
                        <Button style={{ backgroundColor: "transparent", color: "red", borderColor: "transparent" }}
                            onClick={this.changeFormType} name="forgetForm">Forget Password</Button>
                        <Button type="primary" htmlType="submit" className="login-form-button" onClick={this.formAction}>
                            Log in
                        </Button>
                        <br />

                        <hr style={{ marginTop: "15px", width: "40%", float: "left" }} />
                        <span style={{ marginLeft: "15px", color: "white" }}> Or</span>
                        <hr style={{ marginTop: "15px", width: "40%", float: "right" }} />
                        <FacebookLogin
                            appId="495738297582798"
                            icon="fa-facebook"
                            fields="name,email,picture"
                            textButton=" Login with Facebook"
                            onClick={componentClicked}
                            cssClass="btnFacebook"
                            callback={responseFacebook} />
                        <br />
                        <GoogleLogin
                            clientId="603653275276-m2kikrvummnia8qdv2le942rrkbbopps.apps.googleusercontent.com"
                            buttonText="Login with Google"
                            onSuccess={responseGoogle}
                            className='btnGoogle'
                            onFailure={responseGoogle}
                        />
                        <label style={{ color: "white" }}>Create new account</label>
                        <Button style={{ backgroundColor: "transparent", color: "green", borderColor: "transparent" }}
                            onClick={this.changeFormType} name="signup">
                            register now!
                        </Button><br />
                        <label style={{ color: "white" }}>Reset Password ?</label>
                        <Button style={{ backgroundColor: "transparent", color: "green", borderColor: "transparent" }}
                            onClick={this.changeFormType} name="reset-password">
                            click here
                        </Button>
                    </FormItem>
                </Form></section>)
        }

        if (this.state.formType == "forgetForm") {
            return (<section className="bg-img">
                <Form onSubmit={this.handleSubmit} className="login-form">
                    <FormItem style={{ color: "white", fontSize: "25px", textAlign: "center" }}> Forget Password</FormItem>
                    <FormItem>
                        <Input
                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="Enter Your Registered Email"
                            value={this.state.email}
                            onChange={this.handleFormChange.bind(0, 'email')} />
                    </FormItem>
                    <FormItem>
                        <Button type="primary" className="login-form-button" onClick={this.formAction}>Email me</Button>
                        <label style={{ color: "white" }}> Back to login ?</label>
                        <Button style={{ backgroundColor: "transparent", color: "green", borderColor: "transparent" }} onClick={this.changeFormType} name="login">
                            Click here
                        </Button>
                    </FormItem>
                </Form>
            </section>)
        }

        if (this.state.formType == "signup") {
            return (<section className="bg-img">
                <Form onSubmit={this.handleSubmit} className="login-form">
                    <FormItem style={{ color: "white", fontSize: "25px", textAlign: "center" }}>Sign Up</FormItem>
                    <FormItem>
                        <Input
                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            value={this.state.email}
                            placeholder="Enter Your Registered mail Id!"
                            onChange={this.handleFormChange.bind(0, 'email')} />
                    </FormItem>
                    <FormItem>
                        <Input
                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            type="password"
                            placeholder="Password"
                            value={this.state.password}
                            onChange={this.handleFormChange.bind(0, 'password')} />
                    </FormItem>
                    <FormItem>
                        <Input
                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            type="password"
                            placeholder="Confirm Password"
                            value={this.state.confirmPassword}
                            onChange={this.handleFormChange.bind(0, 'confirmPassword')} />
                    </FormItem>
                    <FormItem>
                        <Button type="primary" className="login-form-button" onClick={this.formAction}>Sign-Up</Button>
                        <hr style={{ marginTop: "15px", width: "40%", float: "left" }} />
                        <span style={{ marginLeft: "15px", color: "white", "vertical-align": "middle", "text-align": "center", "font-size": ".85em" }}> Or</span>
                        <hr style={{ marginTop: "15px", width: "40%", float: "right" }} />
                        <label style={{ color: "white" }}> Alredy registered?</label>
                        <Button style={{ backgroundColor: "transparent", color: "green", borderColor: "transparent" }} onClick={this.changeFormType} name="login">
                            Click here
                       </Button>
                    </FormItem>
                </Form>
            </section>)
        }

        if (this.state.formType == "reset-password") {
            return (<section className="bg-img">
                <Form className="login-form">
                    <FormItem style={{ color: "white", fontSize: "25px", textAlign: "center" }}> Reset Password</FormItem>
                    <FormItem>
                        <label style={{ color: "white" }}>Email-Id</label>
                        <Input
                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            value={this.state.email}
                            placeholder="Email"
                            onChange={this.handleFormChange.bind(0, 'email')} />
                    </FormItem>
                    <label style={{ color: "white" }}>Current Password</label>
                    <FormItem>
                        <Input
                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            value={this.state.oldPassword}
                            type="password"
                            placeholder="Current Password"
                            onChange={this.handleFormChange.bind(0, 'oldPassword')} />
                    </FormItem>
                    <label style={{ color: "white" }}>New Password</label>
                    <FormItem>
                        <Input
                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            value={this.state.newPassword}
                            type="password"
                            placeholder="Enter New Password"
                            onChange={this.handleFormChange.bind(0, 'newPassword')} />
                    </FormItem>

                    <FormItem>
                        <Button type="primary" htmlType="submit" className="login-form-button"
                            onClick={this.formAction}> Submit</Button>
                        <label style={{ color: "white" }}> want to login ?</label>
                        <Button style={{ backgroundColor: "transparent", color: "green", borderColor: "transparent" }} onClick={this.changeFormType} name="login">
                            Click here
                       </Button>
                    </FormItem>
                </Form>
            </section>)
        }

    }
}