import React from 'react';
import { Button } from 'antd';
import { logOut } from '../../logout'
import WebEditorPage from '../WebEditor/WebEditorPage'
import { Layout, Menu, Icon } from 'antd';

const { Header, Sider, Content } = Layout;

class App extends React.Component {
    constructor(props) {
        super(props); this.state = {
            collapsed: false
        }
        this.logOut = this.logOut.bind(this);
    }
    logOut(e) {
        e.preventDefault()
        logOut()
    }
    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }
    render() {
         const User= this.props.kcUser
        return (
            <Layout className="home-layout">
                <Sider trigger={null} collapsible collapsed={this.state.collapsed} >
                    <div className="logo" />
                        <div style={{color:"lightblue",textAlign:"center",fontSize:"20px"}}>
                             <b><i>Web </i></b>Editor
                        </div>
                    <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                        <Menu.Item key="1">
                            <Icon type="user" />
                            <span>nav 1</span>
                        </Menu.Item>    
                        <Menu.Item key="2">
                            <Icon type="video-camera" />
                            <span>nav 2</span>
                        </Menu.Item>
                        <Menu.Item key="3">
                            <Icon type="upload" />
                            <span>nav 3</span>
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout>
                    <Header style={{ background: '#fff', padding: 0 }}>
                        <Icon className="trigger" type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'} onClick={this.toggle} />
                       <section style={{float:"right"}}>
                       <Button style={{margin: '25px 15px',float:"right",backgroundColor:"black" ,color:"white" }}  onClick={this.logOut}>
                        <label >Sign Out</label>
                        <Icon  style={{marginLeft:'5px' ,color:"white"}} type="logout" theme="outlined" title="Log out" />
                   </Button>
                   </section>
                    </Header>
                    <div> <span style={{color:"black",float:'right',margin: '5px 15px'}}>
                   {User}</span></div>
                    <Content style={{ margin: '10px 15px', padding: '24px', background: 'Black', minHeight: 280 }}>
                        <WebEditorPage/>
                        </Content>
                </Layout>
            </Layout>
        )
    }
}

export default App;