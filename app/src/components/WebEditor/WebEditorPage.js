import React from 'react';
import { Input ,Button} from 'antd';
import { width } from 'window-size';

const { TextArea } = Input;

class WebEditorPage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }
    render() {
        return (
            <section>
                <section style={{height:"80px",backgroundColor:"white"}}></section>
                <section style={{marginTop:"2px",width:"500px",float:"left"}}>
                    <TextArea placeholder="Only 100 words of Limits"  rows={20}/>
                </section>
                <section style={{marginTop:"2px",width:"470px",float:"right"}}>
                    <TextArea placeholder="This section is for display results." rows={20}/>
                </section>
                <section style={{marginTop:"1px",float:"left" ,shapeMargin:"10px"}}>
                <Button style={{margin:"2px"}} type="primary">Submit</Button>
                <Button style={{margin:"2px"}} type="primary">Upload</Button>
                <Button style={{margin:"2px"}} type="primary">Clear</Button>
                </section>
                <section style={{marginTop:"1px",float:"right" ,shapeMargin:"10px"}}>
                <Button style={{margin:"2px"}} type="primary">Translate</Button>
                <Button style={{margin:"2px"}} type="primary">Download</Button>
                <Button style={{margin:"2px"}} type="primary">New</Button>
                </section>


            </section>
        )
    }
}

export default WebEditorPage;