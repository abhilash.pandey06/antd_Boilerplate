/* react stuf */
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import "../styles/app.css";

/* routing stuf */
import {BrowserRouter as Router} from 'react-router-dom'
import  Route from 'react-router-dom/Route'
import AuthUser from './components/LoginPage/AuthUser';

class Boilerplate extends Component {
  render() {
    return (
        <Router>
          <Route path='/' extact strict component={AuthUser}/>
        </Router>
    )
  }
}

ReactDOM.render(<Boilerplate />, 
  document.getElementById('root')
  );





