const webpack =require('webpack');
const path =require('path');

process.noDeprecation = true 

const GLOBALS = {
  'process.env.NODE_ENV': JSON.stringify('development'),
  __DEV__: false
}

module.exports={

  devtool: 'cheap-source-map',

  resolve: {
    enforceExtension: false
  },

  context: path.resolve(__dirname, './app'),

  /**
   * Entry point settings
   */
  entry: [
    'webpack-hot-middleware/client?reload=true',
    './src/main.js',
  ],

  output: {
    path: path.resolve(__dirname, '/dist'),
    publicPath: '/',
    filename: 'bundle.js'
  },

  target: 'web',
  node: {fs: 'empty'},

  /**
   * Dev server configuration
   */
  devServer: {
    contentBase: path.resolve(__dirname, 'app')
  },

  /**
   * Dev plugins
   */
  plugins: [
    new webpack.DefinePlugin(GLOBALS),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.ProvidePlugin({
      jQuery: 'jquery',
      $: 'jquery'
    })
  ],

  /**
   * Module loader configurations
   */
    module: {
      rules: [{
          test: /\.js$/,
          include: [ path.resolve(__dirname, 'app/src') ],
          use: [{ 
            loader: 'babel-loader',
            options: { cacheDirectory: './.cache' }
          }]
        },
        // {
        //   test: /keycloak\.js$/, 
        //   use: [{
        //     loader: 'string-replace-loader',
        //     query: {
        //       search: '\.withCredentials[ \t]*=[ \t]*true',
        //       replace: '\.withCredentials = false',
        //       flags: 'g'
        //     }
        //   }]
        // },
        { test: /\.json$/, use: ["json-loader"] },

        { test: /\.css$/, use: ['style-loader', 'css-loader'] },
        { test: /\.(sass|scss)$/, use: ["style-loader", "css-loader", "postcss-loader", "sass-loader"] },
        
        {test: /\.(jpe?g)?$/, use: [{ loader: 'url-loader', options:{ mimetype: 'image/jpeg'} } ] },
        {test: /\.(png)$/, use: [{ loader: 'url-loader', options: { mimetype: 'image/png' } } ] },
        {test: /\.(gif)$/, use: [{ loader: 'url-loader', options: { mimetype: 'image/gif' } } ] },
        {test: /\.(svg)$/, use: [{ loader: 'url-loader', options: { mimetype: 'image/svg' } } ] },
        
        { test: /\.ttf(.*)$/, use: [{ loader: 'url-loader', options: { prefix: "fonts", limit:5000 } }]},
        { test: /\.eot(.*)$/, use: [{ loader: 'url-loader', options: { prefix: "fonts", limit:5000 } }]},
        { test: /\.(woff|woff2)(.*)$/, use: [{ loader: 'url-loader', options: { prefix: "fonts", limit:5000 } }]}
      ]
    }
};



//
